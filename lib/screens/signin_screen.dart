import '../providers/auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../locale/app_localization.dart';
import '../models/http_exceptions.dart';
import 'tab_view_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  var _isLoading = false;
  Map<String, String> _authData = {
    'username': '',
    'password': '',
  };

  @override
  Widget build(BuildContext context) {
    void _showErrorDialog(String message) {
      showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text(AppLocalizations.of(context).translate('login_error_title')),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(ctx).pop();
              },
            )
          ],
        ),
      );
    }

    Future<void> _submit() async {
      if (!_formKey.currentState.validate()) {
        // Invalid!
        return;
      }
      _formKey.currentState.save();
      setState(() {
        _isLoading = true;
      });
      try {
        await Provider.of<Auth>(context, listen: false).login(
          _authData['username'],
          _authData['password'],
        );

      } on HttpException catch (error) {
        print(error.toString());
        var errorMessage = 'Authentication failed';
        if (error.toString().contains('Please, enter valid credentials')) {
          errorMessage = AppLocalizations.of(context).translate('login_error_message');
        }
        _showErrorDialog(errorMessage);
      } catch (error) {
        var errorMessage = AppLocalizations.of(context).translate('connection_error');
        _showErrorDialog(errorMessage);
      }

      setState(() {
        _isLoading = false;
      });
    }

    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(30),
        color: Theme.of(context).primaryColor,
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Text(AppLocalizations.of(context).translate('login'), style: Theme.of(context).textTheme.title,),
              ),
              SizedBox(height: 30,),
              TextFormField(
                decoration: InputDecoration(
                    fillColor: Colors.white,
                    filled: true,
                    labelText:
                        AppLocalizations.of(context).translate('username')),
                keyboardType: TextInputType.text,
                onSaved: (value) {
                  _authData['username'] = value;
                },
              ),
              SizedBox(height: 30,),
              TextFormField(
                decoration: InputDecoration(
                    fillColor: Colors.white,
                    filled: true,
                    labelText:
                        AppLocalizations.of(context).translate('password')),
                obscureText: true,
                onSaved: (value) {
                  _authData['password'] = value;
                },
              ),
              SizedBox(
                height: 20,
              ),
              _isLoading ? CircularProgressIndicator()
                :RaisedButton(
                  child: Text(AppLocalizations.of(context).translate('login')),
                  onPressed: _submit,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  padding:
                      EdgeInsets.symmetric(horizontal: 30.0, vertical: 8.0),
                  color: Colors.white,
                  textColor: Colors.black,
                ),
            ],
          ),
        ),
      ),
    );
  }
}
