import '../graphql_operation/mutations/mutations.dart';
import '../models/http_exceptions.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../models/customer.dart';
import '../services/graphQLConf.dart';
import '../graphql_operation/queries/queries.dart';


class Customers with ChangeNotifier {
  final String authToken;
  List<Customer> _customers = [];

  Customers(this.authToken, this._customers);

  List<Customer> get customers {
    return [..._customers];
  }
  Customer customerById(String id){
    return _customers.firstWhere((element) {
      return element.id == id;
    });
  }
  Future<void> fetchAndSetCustomers() async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    QueryResult result = await _client.query(
      QueryOptions(documentNode: gql(QueryAll().getCustomersAll())),
    );
    final List<Customer> _loadedCustomers = [];
    for (var i = 0; i < result.data['allCustomers'].length; i++) {
      _loadedCustomers.add(Customer(
        id: result.data['allCustomers'][i]['id'],
        name: result.data['allCustomers'][i]['name'],
        address: result.data['allCustomers'][i]['address'],
        email1: result.data['allCustomers'][i]['email1'],
        email2: result.data['allCustomers'][i]['email2'],
        phone1: result.data['allCustomers'][i]['phone1'],
        phone2: result.data['allCustomers'][i]['phone2'],
        extraInfo: result.data['allCustomers'][i]['extraInfo'],
        notes: result.data['allCustomers'][i]['note'],
      ));
    }
    _customers = _loadedCustomers;
    notifyListeners();
  }

  Future <void> newCustomerAdd(Customer customer) async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult mutationResult = await _client.mutate(MutationOptions(
          documentNode: gql(MutationsAll().addCustomer(
              customer.name,
              customer.email1,
              customer.email2,
              customer.address,
              customer.extraInfo,
              customer.notes,
              customer.phone1,
              customer.phone2))
      ),
      );
      if (mutationResult.hasException) {
        print(mutationResult.exception);
        throw HttpException('Could not add customer.');
      }
      else {
        customer.id = mutationResult.data['createCustomer']['customer']['id'];
        _customers.add(customer);
        notifyListeners();
      }
    }
    catch (error) {
      throw error;
    }
  }

  Future <void> removeCustomers(String id) async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult mutationResult = await _client.mutate(MutationOptions(
          documentNode: gql(MutationsAll().deleteCustomer(id))));
      if (mutationResult.hasException) {
        print(mutationResult.exception);
        throw HttpException('Could not Delete customer.');
      }
      else {
        _customers.removeWhere((customer) {
          return customer.id == id;
        });
        notifyListeners();
      }

    }
    catch(error){
      throw error;
    }
  }

  Future<void> editCustomers(String id, Map<String, String> editMap) async{
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult mutationResult = await _client.mutate(MutationOptions(
          documentNode: gql(MutationsAll().editCustomer(id, editMap))));
      if (mutationResult.hasException) {
        print(mutationResult.exception);
        throw HttpException('Could not Delete customer.');
      }
      else {
        await fetchAndSetCustomers();
        notifyListeners();
      }

    }
    catch(error){
      throw error;
    }
  }
}