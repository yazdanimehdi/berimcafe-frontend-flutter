import '../models/personnel.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../services/graphQLConf.dart';
import '../models/http_exceptions.dart';
import '../graphql_operation/queries/queries.dart';
import 'package:flutter/foundation.dart';

class RolesProvider with ChangeNotifier {
  final String authToken;
  List<Roles> _rolesList;

  RolesProvider(this.authToken, this._rolesList);

  List<Roles> get getRolesList {
    return [..._rolesList];
  }

  Future<void> getAndFetchRoles() async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    final List<Roles> _loadedRoles = [];
    try {
      QueryResult result = await _client.query(
        QueryOptions(documentNode: gql(QueryAll().getRolesAll())),
      );
      if (!result.hasException) {
        for (var i = 0; i < result.data['allRoles'].length; i++) {
          List<Permission> _permissionList = [];
          for (var j = 0;
          j < result.data['allRoles'][i]['permissionsroleSet'].length;
          j++) {
            Permission _permission = Permission(
                result
                    .data['allRoles'][i]['permissionsroleSet'][j]['permission']
                ['id'],
                result
                    .data['allRoles'][i]['permissionsroleSet'][j]['permission']
                ['permission']);
            _permissionList.add(_permission);
          }
          Roles _role = Roles(
              id: result.data['allRoles'][i]['id'],
              name: result.data['allRoles'][i]['name'],
              roleRelated: result.data['allRoles'][i]['roleRelated'],
              permissionList: _permissionList);
          _loadedRoles.add(_role);
        }
        _rolesList = _loadedRoles;
        notifyListeners();
      } else {
        throw (HttpException(result.exception.toString()));
      }
    }
    catch(error){
      print(error);
      throw(error);
    }
  }
}
