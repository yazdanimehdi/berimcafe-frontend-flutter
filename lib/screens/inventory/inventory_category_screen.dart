import '../../models/inventory.dart';
import '../../providers/foods_all.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import '../../providers/ingredients.dart';
import '../../providers/inventory.dart';
import 'add_inventory_entry_screen.dart';
import 'add_inventory_out_screen.dart';

class InventoryCategoryScreen extends StatefulWidget {
  static const routeName = '/inventory';

  @override
  _InventoryCategoryScreenState createState() =>
      _InventoryCategoryScreenState();
}

class _InventoryCategoryScreenState extends State<InventoryCategoryScreen> {
  var _isLoading = false;
  var _isInit = true;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<Inventory>(context).fetchAndSetInventory().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute
        .of(context)
        .settings
        .arguments as Map;
    final categoryId = arguments['index'];
    final MaterialColor _color = arguments['color'];
    IngredientsCategory _category =
    Provider.of<IngredientsProvider>(context).getCategoriesById(categoryId);
    List<InventoryIngredient> _inventory =
    Provider.of<Inventory>(context).getInventoryByCategory(categoryId);

    return Scaffold(
      appBar: AppBar(
        title: Text(_category.name),
        backgroundColor: _color,
      ),
      body: _isLoading
          ? Center(
        child: CircularProgressIndicator(),
      )
          : Padding(
        padding: EdgeInsets.all(30),
        child: ListView.separated(
            itemBuilder: (BuildContext ctx, int index) {
              return ListTile(
                title: Text(Provider
                    .of<IngredientsProvider>(context)
                    .getIngredientsById(_inventory[index].ingredientId)
                    .name),
                trailing: Column(
                  children: <Widget>[
                    Text(_inventory[index].amount.toString()),
                    Text(_inventory[index].scale)
                  ],
                ),
              );
            },
            separatorBuilder: (context, index) =>
                Divider(
                  height: 20,
                  color: Colors.black26,
                ),
            itemCount: _inventory.length),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          FloatingActionButton(
            heroTag: "btn1",
            backgroundColor: Colors.green,
            child: Icon(
              Icons.add,
              color: Colors.white,
            ),
            onPressed:() {
              Navigator.of(context).pushNamed(AddInventoryEntryScreen.routeName, arguments: {
                'index': categoryId,
                'color': _color,
              });
            },
          ),
          SizedBox(
            height: 10,
          ),
          FloatingActionButton(
            heroTag: "btn2",
            backgroundColor: Colors.red,
            child: Icon(
              Icons.remove,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.of(context).pushNamed(AddInventoryOutScreen.routeName, arguments: {
                'index': categoryId,
                'color': _color,
              });
            },
          )
        ],
      ),
    );
  }
}
