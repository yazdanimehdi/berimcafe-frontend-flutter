import 'package:cafe_management_app/locale/app_localization.dart';
import 'package:flutter/services.dart';
import '../../providers/ingredients.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/http_exceptions.dart';

class AddIngredientScreen extends StatefulWidget {
  static const routeName = '/add-ingredient';

  @override
  _AddIngredientScreenState createState() => _AddIngredientScreenState();
}

class _AddIngredientScreenState extends State<AddIngredientScreen> {
  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> _formKey = GlobalKey();
    final arguments = ModalRoute.of(context).settings.arguments as Map;
    final categoryIndex = arguments['index'];
    final MaterialColor _color = arguments['color'];
    Map<String, String> _formData = {
      "name": '',
      "description": "",
      "scale": "",
      "price": "",
      "category_id":  categoryIndex,
    };
    var _isLoading = false;

    void _showErrorDialog(String message) {
      showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title:
              Text(AppLocalizations.of(context).translate('login_error_title')),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(ctx).pop();
              },
            )
          ],
        ),
      );
    }

    Future<void> _submit() async {
      if (!_formKey.currentState.validate()) {
        // Invalid!
        return;
      }
      _formKey.currentState.save();
      setState(() {
        _isLoading = true;
      });
      try {
        await Provider.of<IngredientsProvider>(context, listen: false)
            .addIngredient(_formData);
      } on HttpException catch (error) {
        print(error.toString());
        var errorMessage = 'Editing failed';
        if (error.toString().contains('Please, enter valid credentials')) {
          errorMessage =
              AppLocalizations.of(context).translate('login_error_message');
        }
        _showErrorDialog(errorMessage);
      } catch (error) {
        print(error);
        var errorMessage =
            AppLocalizations.of(context).translate('connection_error');
        _showErrorDialog(errorMessage);
      }

      setState(() {
        _isLoading = false;
      });
      Navigator.of(context, rootNavigator: true)..pop();
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).translate('add_ingredient')),
        backgroundColor: _color,
        actions: <Widget>[
          FlatButton(
            onPressed: () {},
            child: Text(
              AppLocalizations.of(context).translate('submit'),
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(30),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        onSaved: (String value) {
                          _formData['name'] = value;
                        },
                        validator: (String value) {
                          return value.length < 1
                              ? AppLocalizations.of(context)
                                  .translate('input_field_valid')
                              : null;
                        },
                        decoration: InputDecoration(
                            labelText:
                                AppLocalizations.of(context).translate('name') +
                                    "*"),
                      ),
                      TextFormField(
                        validator: (String value) {
                          int _intValue;
                          try{
                            _intValue = int.parse(value);
                          }
                          catch(error){
                            return AppLocalizations.of(context)
                                .translate('input_english');
                          }
                          return value.length < 1
                              ? AppLocalizations.of(context)
                                  .translate('input_field_valid')
                              : null;
                        },
                        keyboardType: TextInputType.number,
                        onSaved: (value) {
                          _formData['price'] = value;
                        },
                        decoration: InputDecoration(
                            labelText: AppLocalizations.of(context)
                                    .translate('price') +
                                "*"),
                      ),
                      TextFormField(
                        onSaved: (value) {
                          _formData['description'] = value;
                        },
                        decoration: InputDecoration(
                            labelText: AppLocalizations.of(context)
                                .translate('description')),
                      ),
                      TextFormField(
                        validator: (String value) {
                          return value.length < 1
                              ? AppLocalizations.of(context)
                                  .translate('input_field_valid')
                              : null;
                        },
                        onSaved: (value) {
                          _formData['scale'] = value;
                        },
                        decoration: InputDecoration(
                            labelText: AppLocalizations.of(context)
                                    .translate('scale') +
                                "*"),
                      ),
                      RaisedButton(
                        onPressed: _submit,
                        child: Text(
                            AppLocalizations.of(context).translate('submit')),
                      )
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
