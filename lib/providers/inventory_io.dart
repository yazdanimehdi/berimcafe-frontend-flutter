import '../graphql_operation/mutations/mutations.dart';
import 'package:flutter/foundation.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../models/inventory.dart';
import '../models/http_exceptions.dart';
import '../services/graphQLConf.dart';
import '../graphql_operation/queries/queries.dart';

class InventoryIO with ChangeNotifier {
  final String authToken;
  List<InventoryEntry> _inventoryEntry;
  List<InventoryOut> _inventoryOut;

  InventoryIO(this.authToken, this._inventoryEntry, this._inventoryOut);

  List<InventoryEntry> get getInventoryEntry {
    return [..._inventoryEntry];
  }

  List<InventoryOut> get getInventoryOut {
    return [..._inventoryOut];
  }

  List<Object> get getInventoryIO{
    List<Object> _listAll = [];
    for(var i = 0; i < _inventoryEntry.length; i++){
      _listAll.add(_inventoryEntry[i]);
    }
    for(var j = 0; j < _inventoryOut.length; j++){
      _listAll.add(_inventoryOut[j]);
    }
    _listAll.sort((obj1, obj2){
      InventoryEntry _invEntry1;
      InventoryOut _invOut1;

      InventoryEntry _invEntry2;
      InventoryOut _invOut2;
      if(obj1 is InventoryEntry){
        _invEntry1 = obj1;
      }
      else{
        _invOut1 = obj1;
      }

      if(obj2 is InventoryEntry){
        _invEntry2 = obj2;
      }
      else{
        _invOut2 = obj2;
      }

      if (_invEntry1 == null && _invEntry2 == null){
        if(_invOut1.dateTime.isAfter(_invOut2.dateTime)){
          return -1;
        }
        else{
          return 1;
        }
      }

      if (_invOut1 == null && _invEntry2 == null){
        if(_invEntry1.dateTime.isAfter(_invOut2.dateTime)){
          return -1;
        }
        else{
          return 1;
        }
      }

      if (_invEntry1 == null && _invOut2 == null){
        if(_invOut1.dateTime.isAfter(_invEntry2.dateTime)){
          return -1;
        }
        else{
          return 1;
        }
      }

      if (_invOut1 == null && _invOut2 == null){
        if(_invEntry1.dateTime.isAfter(_invEntry2.dateTime)){
          return -1;
        }
        else{
          return 1;
        }
      }

      return 0;
    });
    return _listAll;
  }

  Future<void> getAndFetchInventoryIO() async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    final List<InventoryEntry> _loadedInventory = [];
    try {
      QueryResult result = await _client.query(
        QueryOptions(documentNode: gql(QueryAll().getInventoryEntries())),
      );
      if (!result.hasException) {
        if (result.data['allInventoryEntry'] == null) {
          _inventoryEntry = [];
        }
        for (var i = 0; i < result.data['allInventoryEntry'].length; i++) {
          InventoryEntry _inv;
          _inv = InventoryEntry(
              id: result.data['allInventoryEntry'][i]['id'],
              amount: result.data['allInventoryEntry'][i]['amount'],
              ingredientId: result.data['allInventoryEntry'][i]['ingredient']
                  ['id'],
              cost: result.data['allInventoryEntry'][i]['cost'],
              description: result.data['allInventoryEntry'][i]['description'],
              dateTime: DateTime.parse(
                  result.data['allInventoryEntry'][i]['dateTime']));
          _loadedInventory.add(_inv);
        }
        _inventoryEntry = _loadedInventory;

        notifyListeners();
        try {
          QueryResult resultOut = await _client.query(
            QueryOptions(documentNode: gql(QueryAll().getInventoryOuts())),
          );
          if (!result.hasException) {
            if (resultOut.data['allInventoryOut'] == null) {
              _inventoryOut = [];
            } else {
              List<InventoryOut> _loadedInventory = [];
              for (var i = 0;
                  i < resultOut.data['allInventoryOut'].length;
                  i++) {
                InventoryOut _inv = InventoryOut(
                    id: resultOut.data['allInventoryOut'][i]['id'],
                    ingredientId: resultOut.data['allInventoryOut'][i]
                        ['ingredient']['id'],
                    amount: resultOut.data['allInventoryOut'][i]['amount'],
                    description: resultOut.data['allInventoryOut'][i]
                        ['description'],
                    dateTime: DateTime.parse(
                        resultOut.data['allInventoryOut'][i]['dateTime']));
                _loadedInventory.add(_inv);
              }
              _inventoryOut = _loadedInventory;
              notifyListeners();
            }
          } else {
            throw HttpException(resultOut.exception.toString());
          }
        } catch (error) {
          print(error);
          throw (error);
        }
      } else {
        throw HttpException(result.exception.toString());
      }
    } catch (error) {
      print(error);
      throw error;
    }
  }
  Future<void> deleteInventoryEntry(String inventoryEntryId) async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    QueryResult result = await _client.mutate(
      MutationOptions(documentNode: gql(MutationsAll().deleteInventoryEntry(inventoryEntryId))),
    );
    try {
      if (!result.hasException) {
        if(result.data['deleteInventoryEntry']['status']){
          getAndFetchInventoryIO();
        }
        else{
          throw(HttpException('Inventory Entry Does Not Exist!'));
        }
      }
      else {
        throw(HttpException(result.exception.toString()));
      }
    }
    catch (error) {
      print(error);
      throw(error);
    }
  }

  Future<void> deleteInventoryOut(String inventoryOutId) async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    QueryResult result = await _client.mutate(
      MutationOptions(documentNode: gql(MutationsAll().deleteInventoryOut(inventoryOutId))),
    );
    try {
      if (!result.hasException) {
        if(result.data['deleteInventoryOut']['status']){
          getAndFetchInventoryIO();
        }
        else{
          throw(HttpException('Inventory Out Does Not Exist!'));
        }
      }
      else {
        throw(HttpException(result.exception.toString()));
      }
    }
    catch (error) {
      print(error);
      throw(error);
    }
  }
}
