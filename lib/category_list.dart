import './models/category.dart';
import 'package:flutter/material.dart';


const CATEGORIES = const [
   Category(id:1, name: 'order', english: 'Orders', color: Colors.green, icon: Icons.add_shopping_cart),
   Category(id:2, name: 'customers', english: 'Customers',color: Colors.red, icon: Icons.group),
   Category(id:3, name: 'shift', english: 'Shifts',color: Colors.blue, icon: Icons.access_time),
   Category(id:4, name: 'help', english: 'Help',color: Colors.purple, icon: Icons.help),
   Category(id:5, name: 'bar', english: 'Bar',color: Colors.brown, icon: Icons.local_cafe),
   Category(id:6, name: 'kitchen', english: 'Kitchen',color: Colors.amber, icon: Icons.local_dining),
   Category(id:7, name: 'personnel', english: 'Personnel',color: Colors.deepOrange,icon: Icons.transfer_within_a_station),
   Category(id:8, name: 'saloon', english: 'Saloon',color: Colors.pink, icon: Icons.fastfood),
   Category(id:9, name: 'inventory', english: 'Inventory',color: Colors.teal, icon: Icons.store_mall_directory),
   Category(id:10, name: 'roles', english: 'Roles',color: Colors.black, icon: Icons.vpn_key),
   Category(id:11, name:'menu', english: 'Menu',color: Colors.lightGreen, icon: Icons.local_activity),
   Category(id:12, name:'ingredient', english: 'Ingredients',color: Colors.purpleAccent, icon: Icons.local_mall)

];