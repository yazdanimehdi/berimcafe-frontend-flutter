import 'dart:io';

import 'package:cafe_management_app/graphql_operation/subscriptions/subscriptions.dart';
import 'package:cafe_management_app/services/graphQLConf.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'models/chat_models.dart';
import 'providers/inventory_io.dart';

import 'screens/ingredients/ingredient_edit_view_screen.dart';
import 'screens/inventory/inventory_category_screen.dart';
import 'screens/inventory/inventory_screen.dart';
import 'providers/inventory.dart';
import 'screens/foods/food_add_screen.dart';

import 'screens/ingredients/ingredient_category_screen.dart';

import 'providers/ingredients.dart';

import 'screens/customers/view_and_edit_customer_screen.dart';

import './screens/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:overlay_support/overlay_support.dart';
import 'locale/app_localization.dart';
import './screens/tab_view_screen.dart';
import './screens/customers/customers_screen.dart';
import './screens/profile_picture_screen.dart';
import './screens/shifts_screen.dart';
import './screens/order_screen.dart';
import './screens/help_screen.dart';
import './screens/select_language_screen.dart';
import './providers/customers.dart';
import './providers/auth.dart';
import './screens/signin_screen.dart';
import 'screens/customers/add_customers_screen.dart';
import './providers/locale.dart';
import 'screens/foods/food_category_screen.dart';
import './providers/foods.dart';
import 'screens/foods/food_list.dart';
import 'screens/foods/food_edit_view_screen.dart';
import 'screens/ingredients/ingredient_list.dart';
import 'screens/ingredients/add_ingredient_screen.dart';
import 'screens/inventory/inventory_io_screen.dart';
import 'screens/inventory/add_inventory_entry_screen.dart';
import './screens/inventory/add_inventory_out_screen.dart';
import './screens/inventory/inventory_io_filter_screen.dart';
import './screens/personnel/personnel_list_screen.dart';
import './screens/personnel/roles_screen.dart';
import './providers/personnel.dart';
import './providers/roles.dart';
import './providers/chat.dart';
import './screens/chat/chat_screen.dart';
import './screens/chat/new_chat_screen.dart';

import 'dart:async';

import 'package:graphql/client.dart';
import 'package:graphql/internal.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var _init = true;

  StreamSubscription<FetchResult> _subscription;

  void _onError(final Object error) {
    print(error.toString());
  }

  void _initSubscription(String token, Function onData) async {
    GraphQLClient client = GraphQLConfiguration().wsClient();
    assert(client != null);
    if (_subscription != null) {
      _subscription.cancel().then((onValue) {
        print(onValue);
      });
    } else {
      final Stream<FetchResult> stream = client.subscribe(Operation(
        operationName: "chatMessages",
        documentNode: gql(SubscriptionsAll().getMessages(token)),
      ));
      _subscription = stream.listen(
        onData,
        onError: _onError,
        onDone: () {},
      );
    }
  }

  @override
  void initState() {
    _init = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: Auth(),
        ),
        ChangeNotifierProvider.value(
          value: LocaleProvider(),
        ),
        ChangeNotifierProxyProvider<Auth, Customers>(
          create: (_) => Customers(null, []),
          update: (ctx, auth, previousCustomers) => Customers(
            auth.token,
            previousCustomers == null ? [] : previousCustomers.customers,
          ),
        ),
        ChangeNotifierProxyProvider<Auth, FoodsAll>(
          create: (_) => FoodsAll(null, []),
          update: (ctx, auth, previousFoodAll) => FoodsAll(
            auth.token,
            previousFoodAll == null ? [] : previousFoodAll.getCategories,
          ),
        ),
        ChangeNotifierProxyProvider<Auth, IngredientsProvider>(
          create: (_) => IngredientsProvider(null, []),
          update: (ctx, auth, previousIngredientsAll) => IngredientsProvider(
            auth.token,
            previousIngredientsAll == null
                ? []
                : previousIngredientsAll.getIngredients,
          ),
        ),
        ChangeNotifierProxyProvider<Auth, Inventory>(
          create: (_) => Inventory(null, []),
          update: (ctx, auth, previousInventory) => Inventory(
            auth.token,
            previousInventory == null ? [] : previousInventory.getInventories,
          ),
        ),
        ChangeNotifierProxyProvider<Auth, InventoryIO>(
          create: (_) => InventoryIO(null, [], []),
          update: (ctx, auth, previousInventory) => InventoryIO(
            auth.token,
            previousInventory == null
                ? []
                : previousInventory.getInventoryEntry,
            previousInventory == null ? [] : previousInventory.getInventoryOut,
          ),
        ),
        ChangeNotifierProxyProvider<Auth, PersonnelProvider>(
          create: (_) => PersonnelProvider(null, []),
          update: (ctx, auth, previousPersonnel) => PersonnelProvider(
            auth.token,
            previousPersonnel == null ? [] : previousPersonnel.getPersonnelList,
          ),
        ),
        ChangeNotifierProxyProvider<Auth, RolesProvider>(
          create: (_) => RolesProvider(null, []),
          update: (ctx, auth, previousRoles) => RolesProvider(
            auth.token,
            previousRoles == null ? [] : previousRoles.getRolesList,
          ),
        ),
        ChangeNotifierProvider.value(value: ChatProvider())
      ],
      child: OverlaySupport(
        child: Consumer3<LocaleProvider, Auth, ChatProvider>(
          builder: (ctx, locale, auth, chat, child3) {
            if (auth.isAuth) {
              chat.setAuth(auth.token);
              chat.getAndSetChats();

              if (_subscription == null) {
                _initSubscription(auth.token, (message) {
                  var _chatId =
                      message.data['chatMessagesCreatedUpdated']['chat']['id'];
                  ChatMessages messageCame = ChatMessages(
                      read: message.data['chatMessagesCreatedUpdated']['read'],
                      dateTime: DateTime.parse(
                          message.data['chatMessagesCreatedUpdated']['dateTime']),
                      repliedToId: message.data['chatMessagesCreatedUpdated']
                                  ['replyMessage'] ==
                              null
                          ? null
                          : message.data['chatMessagesCreatedUpdated']
                              ['replyMessage']['id'],
                      message: message.data['chatMessagesCreatedUpdated']
                          ['message'],
                      fromId: message.data['chatMessagesCreatedUpdated']
                          ['fromUser']['id'],
                      id: message.data['chatMessagesCreatedUpdated']['id'],
                      loading: false);
                  if (chat.getChatList
                          .firstWhere((chat) {
                            if (chat.id == _chatId) {
                              return true;
                            } else {
                              return false;
                            }
                          })
                          .users[0]
                          .id !=
                      messageCame.fromId) {
                    Provider.of<ChatProvider>(ctx, listen: false)
                        .addChatMessage(messageCame, _chatId);
                  }
                });
              } else {
                _subscription.onData((message) {
                  var _chatId =
                      message.data['chatMessagesCreatedUpdated']['chat']['id'];
                  ChatMessages messageCame = ChatMessages(
                      read: message.data['chatMessagesCreatedUpdated']['read'],
                      dateTime: DateTime.parse(
                          message.data['chatMessagesCreatedUpdated']['dateTime']),
                      repliedToId: message.data['chatMessagesCreatedUpdated']
                                  ['replyMessage'] ==
                              null
                          ? null
                          : message.data['chatMessagesCreatedUpdated']
                              ['replyMessage']['id'],
                      message: message.data['chatMessagesCreatedUpdated']
                          ['message'],
                      fromId: message.data['chatMessagesCreatedUpdated']
                          ['fromUser']['id'],
                      id: message.data['chatMessagesCreatedUpdated']['id'],
                      loading: false);
                  if (chat.getChatList
                          .firstWhere((chat) {
                            if (chat.id == _chatId) {
                              return true;
                            } else {
                              return false;
                            }
                          })
                          .users[0]
                          .id !=
                      messageCame.fromId) {
                    Provider.of<ChatProvider>(ctx, listen: false)
                        .addChatMessage(messageCame, _chatId);
                  }
                  showOverlayNotification((context) {
                    return Text('salam');
                  }, duration: Duration(milliseconds: 4000));
                });
              }
            } else {
              auth.tryAutoLogin();
            }
            return GestureDetector(
              onTap: () {
                FocusScopeNode currentFocus = FocusScope.of(context);

                if (!currentFocus.hasPrimaryFocus) {
                  currentFocus.unfocus();
                }
              },
              child: MaterialApp(
                title: 'Flutter Demo',
                theme: ThemeData(
                    primaryColor: Color.fromRGBO(40, 40, 40, 1),
                    accentColor: Colors.black,
                    fontFamily: 'iransans',
                    textTheme: TextTheme(
                        title: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 22,
                            color: Colors.white))),
                supportedLocales: [
                  Locale('fa', 'IR'),
                  Locale('en', 'US'),
                ],
                localizationsDelegates: [
                  AppLocalizations.delegate,
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                ],
                locale: locale.isSet ? locale.locale : Locale('en', 'US'),
                home: !locale.isSet
                    ? FutureBuilder(
                        future: locale.tryChangeLocale(),
                        builder: (ctx, localeResultSnapshot) =>
                            SelectLanguageScreen(),
                      )
                    : auth.isAuth
                        ? TabViewScreen()
                        : FutureBuilder(
                            future: auth.tryAutoLogin(),
                            builder: (ctx, authResultSnapshot) =>
                                authResultSnapshot.connectionState ==
                                        ConnectionState.waiting
                                    ? SplashScreen()
                                    : LoginScreen(),
                          ),
                routes: {
                  CustomersScreen.routeName: (ctx) => CustomersScreen(),
                  ProfilePictureScreen.routeName: (ctx) => ProfilePictureScreen(),
                  ShiftScreen.routeName: (ctx) => ShiftScreen(),
                  OrderScreen.routName: (ctx) => OrderScreen(),
                  HelpScreen.routeName: (ctx) => HelpScreen(),
                  TabViewScreen.routeName: (ctx) => TabViewScreen(),
                  AddCustomerScreen.routeName: (ctx) => AddCustomerScreen(),
                  ViewEditCustomers.routName: (ctx) => ViewEditCustomers(),
                  FoodCategoryScreen.routName: (ctx) => FoodCategoryScreen(),
                  FoodScreen.routName: (ctx) => FoodScreen(),
                  FoodViewEditScreen.routeName: (ctx) => FoodViewEditScreen(),
                  IngredientsCategoryScreen.routeName: (ctx) =>
                      IngredientsCategoryScreen(),
                  IngredientListScreen.routeName: (ctx) => IngredientListScreen(),
                  AddIngredientScreen.routeName: (ctx) => AddIngredientScreen(),
                  FoodAddScreen.routeName: (ctx) => FoodAddScreen(),
                  IngredientEditViewScreen.routeName: (ctx) =>
                      IngredientEditViewScreen(),
                  InventoryScreen.routeName: (ctx) => InventoryScreen(),
                  InventoryCategoryScreen.routeName: (ctx) =>
                      InventoryCategoryScreen(),
                  InventoryIoScreen.routeName: (ctx) => InventoryIoScreen(),
                  AddInventoryEntryScreen.routeName: (ctx) =>
                      AddInventoryEntryScreen(),
                  AddInventoryOutScreen.routeName: (ctx) =>
                      AddInventoryOutScreen(),
                  InventoryIOFilterScreen.routeName: (ctx) =>
                      InventoryIOFilterScreen(),
                  PersonnelListScreen.routeName: (ctx) => PersonnelListScreen(),
                  RolesScreen.roteName: (ctx) => RolesScreen(),
                  ChatScreen.routeName: (ctx) => ChatScreen(),
                  NewChatScreen.routeName: (ctx) => NewChatScreen(),
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
