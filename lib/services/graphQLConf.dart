import 'dart:convert';

import "package:graphql_flutter/graphql_flutter.dart";


class GraphQLConfiguration {

  static HttpLink httpLink = HttpLink(

    uri: "http://127.0.0.1:8000/graphql/",

  );

  GraphQLClient wsClient() {
    final WebSocketLink websocketLink = WebSocketLink(
      url: 'ws://127.0.0.1:8000/graphql/',
      config: SocketClientConfig(
          autoReconnect: true, inactivityTimeout: Duration(seconds: 1800)),
    );

    final Link _link = websocketLink;


    return GraphQLClient(
      cache: InMemoryCache(),
      link: _link,
    );
  }

  GraphQLClient clientToQuery(String token) {
        Link link = httpLink;
        if(token != null) {
          AuthLink authLink = AuthLink(
            getToken: () => 'JWT ' + token,
          );

           link = authLink.concat(httpLink);
        }

        return GraphQLClient(


          cache: OptimisticCache(dataIdFromObject: typenameDataIdFromObject),

          link: link,

        );

      }
    }

