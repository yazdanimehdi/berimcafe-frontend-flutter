class SubscriptionsAll {
  String getMessages(String token) {
    return """
     subscription chatMessages{
        chatMessagesCreatedUpdated(token:"$token"){
          id
          message
          chat{
            name
            id
            users{
              id
            username
            firstName
            lastName
            rolepersonnelSet{
              role{
                name
              }
            }
           }
            
          }
          fromUser{
            id
          }
          dateTime
          replyMessage{
            id
          }
          read
        }
      }
    """;
  }
}
