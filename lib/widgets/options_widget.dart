import 'dart:ui';

import '../screens/personnel/roles_screen.dart';

import '../screens/ingredients/ingredient_category_screen.dart';
import '../screens/inventory/inventory_screen.dart';

import '../screens/customers/customers_screen.dart';
import '../screens/shifts_screen.dart';
import '../screens/order_screen.dart';
import '../screens/help_screen.dart';
import '../screens/foods/food_category_screen.dart';
import '../screens/personnel/personnel_list_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OptionsWidget extends StatelessWidget {
  final String text;
  final int id;
  final Color color;
  final IconData icon;

  void _tapFunction(BuildContext ctx, int id){
    if(id == 1){
      Navigator.of(ctx).pushNamed(OrderScreen.routName);
    }
    if(id == 2) {
      Navigator.of(ctx).pushNamed(CustomersScreen.routeName);
    }
    if(id == 3) {
      Navigator.of(ctx).pushNamed(ShiftScreen.routeName);
    }
    if(id == 4){
      Navigator.of(ctx).pushNamed(HelpScreen.routeName);
    }
    if(id ==11){
      Navigator.of(ctx).pushNamed(FoodCategoryScreen.routName);
    }
    if(id == 12){
      Navigator.of(ctx).pushNamed(IngredientsCategoryScreen.routeName);
    }
    if(id == 9){
      Navigator.of(ctx).pushNamed(InventoryScreen.routeName);
    }
    if(id == 7){
      Navigator.of(ctx).pushNamed(PersonnelListScreen.routeName);
    }
    if(id == 10){
      Navigator.of(ctx).pushNamed(RolesScreen.roteName);
    }
   
  }
  
  OptionsWidget({this.text, this.id, this.color, this.icon});
  
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => _tapFunction(context, id),
      splashColor: color,
      radius: 300,
      child: Container(
        height: 300,
        width: 300,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          gradient: LinearGradient(
            colors: [color.withOpacity(0.5), color],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
            child:
                Center(
                    child:
                    Stack(
                      children: <Widget>[
                        Icon(icon, size: 50, color: Colors.black12,),
                        Center(
                          child: Text(text, style: TextStyle(color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,),
                        ),
                      ],
                    )),
            ),
          ),
    );
  }
}
