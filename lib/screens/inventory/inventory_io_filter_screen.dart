import '../../locale/app_localization.dart';
import 'package:flutter/material.dart';

class InventoryIOFilterScreen extends StatefulWidget {
  static const routeName = 'inventoryio-filter';
  @override
  _InventoryIOFilterScreenState createState() => _InventoryIOFilterScreenState();
}

class _InventoryIOFilterScreenState extends State<InventoryIOFilterScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).translate('inventory_io_filter')),
        backgroundColor: Colors.teal,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.done, color: Colors.white,),
            onPressed: (){

            },
          )
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(20),

      ),
    );
  }
}
