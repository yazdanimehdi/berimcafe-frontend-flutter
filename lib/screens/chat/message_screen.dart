import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';
import '../../providers/chat.dart';
import '../../models/chat_models.dart';
import 'package:intl/intl.dart';
import './chat_screen.dart';

class MessageScreen extends StatefulWidget {
  @override
  _MessageScreenState createState() => _MessageScreenState();
}

class _MessageScreenState extends State<MessageScreen> {
  var _isLoading = false;
  var _isInit = true;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<ChatProvider>(context).getAndSetChats().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    List<Chat> _chats = Provider.of<ChatProvider>(context).getChatList;

    return _isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                floating: true,
                expandedHeight: 50,
                backgroundColor: Theme.of(context).primaryColor,
                flexibleSpace: FlexibleSpaceBar(
                  titlePadding:
                      EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                  title: TextField(
                    style: TextStyle(color: Colors.white),
                    textAlign: TextAlign.center,
                    cursorColor: Colors.white,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Theme.of(context).accentColor,
                      disabledBorder: InputBorder.none,
                      alignLabelWithHint: true,
                      hintText: 'Search for Users and Messages',
                      hintStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      icon: Icon(
                        Icons.search,
                        color: Colors.white,
                      ),
                      contentPadding:
                          const EdgeInsets.only(bottom: 8, left: 10, right: 10),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                ),
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate(
                  (BuildContext ctx, int index) {
                    return Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Theme.of(context).dividerColor))),
                      child: ListTile(
                        onTap: () {
                          Navigator.of(context).pushNamed(ChatScreen.routeName, arguments: _chats[index].id);
                        },
                        isThreeLine: true,
                        contentPadding: EdgeInsets.only(top: 0, right: 10),
                        leading: CircleAvatar(
                          radius: 30,
                          backgroundColor: Colors.black,
                          foregroundColor: Colors.black,
                        ),
                        title: Row(
                          children: <Widget>[
                            Text(_chats[index].name == null ? '${_chats[index].users[_chats[index].users.length - 1].firstName} ${_chats[index].users[_chats[index].users.length - 1].lastName}' : _chats[index].name),
                            Icon(Icons.volume_down),
                          ],
                        ),
                        subtitle: _chats[index].name != null ? Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "${Provider.of<ChatProvider>(context, listen: false).getChatUser(_chats[index].messages[_chats[index].messages.length - 1].fromId).firstName} ${Provider.of<ChatProvider>(context, listen: false).getChatUser(_chats[index].messages[_chats[index].messages.length - 1].fromId).lastName}",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Text(
                              _chats[index].messages[_chats[index].messages.length - 1].message == null ? "" : _chats[index].messages[_chats[index].messages.length - 1].message,
                            )
                          ],
                        ):Text(_chats[index].messages[_chats[index].messages.length - 1].message == null ? "" : _chats[index].messages[_chats[index].messages.length - 1].message),
                        trailing: Padding(
                          padding: const EdgeInsets.only(left: 8),
                          child: Column(
                            children: <Widget>[
                              Text(DateFormat.Hm().format(_chats[index].messages[_chats[index].messages.length - 1].dateTime).toString()),
                              Container(
                                height: 20,
                                width: 20,
                                decoration: BoxDecoration(color: Colors.blue),
                                child: Text(
                                  Provider.of<ChatProvider>(context).newMessages(_chats[index].id).toString(),
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  childCount: _chats.length,
                ),
              )
            ],
          );
  }
}
