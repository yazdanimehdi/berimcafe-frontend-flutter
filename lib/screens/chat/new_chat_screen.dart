import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/personnel.dart';
import '../tab_view_screen.dart';

class NewChatScreen extends StatefulWidget {
  static const routeName = 'new-chat';

  @override
  _NewChatScreenState createState() => _NewChatScreenState();
}

class _NewChatScreenState extends State<NewChatScreen> {
  var _isInit = true;
  var _isLoading = false;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      Provider.of<PersonnelProvider>(context).getAndFetchPersonnel().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var personnel = Provider.of<PersonnelProvider>(context).getPersonnelList;
    return _isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : Scaffold(
            body: CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  floating: true,
                  pinned: true,
                  title: Text('New Message'),
                  actions: <Widget>[
                    FlatButton(
                      child: Text(
                        'Cancel',
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        Navigator.of(context).pushReplacementNamed(
                            TabViewScreen.routeName,
                            arguments: 1);
                      },
                    )
                  ],
                ),
                SliverAppBar(
                  floating: true,
                  expandedHeight: 50,
                  primary: false,
                  backgroundColor: Theme.of(context).primaryColor,
                  flexibleSpace: FlexibleSpaceBar(
                    titlePadding:
                        EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                    title: TextField(
                      style: TextStyle(color: Colors.white),
                      textAlign: TextAlign.center,
                      cursorColor: Colors.white,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Theme.of(context).accentColor,
                        disabledBorder: InputBorder.none,
                        alignLabelWithHint: true,
                        hintText: 'Search for Users and Messages',
                        hintStyle: TextStyle(
                          color: Colors.grey,
                        ),
                        icon: Icon(
                          Icons.search,
                          color: Colors.white,
                        ),
                        contentPadding: const EdgeInsets.only(
                            bottom: 8, left: 10, right: 10),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                  ),
                ),
                SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) => Material(
                      child: ListTile(
                        leading: Icon(Icons.group_add),
                        title: Text('New Group'),
                        onTap: () {},
                      ),
                    ),
                    childCount: 1,
                  ),
                ),
                SliverList(
                  delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) =>Divider(),
                    childCount: 1,
                  ),
                ),
                SliverList(
                    delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) => Material(
                              child: ListTile(
                                title: Text(
                                    "${personnel[index].firstName} ${personnel[index].lastName}"),
                                onTap: () {},
                              ),
                            ),
                        childCount: personnel.length))
              ],
            ),
          );
  }
}
