import 'package:cafe_management_app/providers/foods_all.dart';
import 'package:provider/provider.dart';
import '../../providers/ingredients.dart';
import '../../providers/inventory.dart';
import '../../models/http_exceptions.dart';
import '../../locale/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'dart:io' show Platform;

class AddInventoryEntryScreen extends StatefulWidget {
  static const routeName = '/add-inventory-entry';

  @override
  _AddInventoryEntryScreenState createState() =>
      _AddInventoryEntryScreenState();
}

class _AddInventoryEntryScreenState extends State<AddInventoryEntryScreen> {
  var _isLoading = false;
  var _isInit = true;
  List<String> _dropdownValues;
  List<String> _ids;
  String dropdownValueIngredient;
  Map<String, Object> _entryMap = {
    'amount': '0',
    'cost': '0',
    'ingredient_id': '',
    'description': '',
    'date_time': DateTime.now(),
    'ingredientId': '',
  };
  DateTime selectedDate;
  TimeOfDay selectedTime;

  final GlobalKey<FormState> _formKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context).settings.arguments as Map;
    final categoryId = arguments['index'];
    final MaterialColor _color = arguments['color'];
    Map<String, Object> _ingredients = Provider.of<IngredientsProvider>(context)
        .getIngredientsByCategory(categoryId);
    List<Ingredients>_ingredientsAll = _ingredients['all'];
    if (_isInit) {
      _dropdownValues = _ingredients['names'];
      _ids = _ingredients['ids'];
      dropdownValueIngredient = _dropdownValues[0];
      _entryMap['ingredient_id'] = _ids[0];
    }

    void _showErrorDialog(String message) {
      showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title:
              Text(AppLocalizations.of(context).translate('login_error_title')),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text(AppLocalizations.of(context).translate('ok')),
              onPressed: () {
                Navigator.of(ctx).pop();
              },
            )
          ],
        ),
      );
    }

    void _submit() async {
      if (_formKey.currentState.validate()) {
        _formKey.currentState.save();
        _entryMap['date_time'] = DateTime(selectedDate.year, selectedDate.month,
            selectedDate.day, selectedTime.hour, selectedTime.minute);
        try {
          await Provider.of<Inventory>(context, listen: false)
              .addInventoryEntry(
                  _entryMap['description'],
                  _entryMap['amount'],
                  _entryMap['date_time'],
                  _entryMap['ingredient_id'],
                  _entryMap['cost']);
          Navigator.of(context).pop();
        } on HttpException catch (_) {
          var errorMessage =
              AppLocalizations.of(context).translate('connection_error');
          _showErrorDialog(errorMessage);
        }
        setState(() {
          _isLoading = false;
        });
      }
    }

    void _presentDatePickerAndroid() {
      showDatePicker(
              context: context,
              initialDate: DateTime.now(),
              firstDate: DateTime.now().subtract(Duration(days: 30)),
              lastDate: DateTime.now())
          .then((chosenDate) {
        if (chosenDate == null) {
          return;
        }
        selectedDate = chosenDate;
      });
    }

    void _presentDatePickerIOS() {
      showCupertinoModalPopup(
          context: context,
          builder: (BuildContext ctx) {
            return Container(
                height: MediaQuery.of(context).copyWith().size.height / 3,
                width: MediaQuery.of(context).copyWith().size.width,
                color: Colors.white,
                child: Column(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).copyWith().size.height *
                          4 /
                          15,
                      child: CupertinoDatePicker(
                        initialDateTime: DateTime.now(),
                        onDateTimeChanged: (DateTime newdate) {
                          setState(() {
                            selectedDate = DateTime(
                                newdate.year, newdate.month, newdate.day);
                          });
                        },
                        use24hFormat: true,
                        maximumDate: DateTime.now(),
                        minimumDate:
                            DateTime.now().subtract(Duration(days: 30)),
                        mode: CupertinoDatePickerMode.date,
                      ),
                    ),
                    Container(
                      height:
                          MediaQuery.of(context).copyWith().size.height / 15,
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          FlatButton(
                            child: Text(
                              AppLocalizations.of(context).translate('submit'),
                              style: TextStyle(color: Colors.green),
                            ),
                            onPressed: () {
                              if (selectedDate == null) {
                                setState(() {
                                  selectedDate = DateTime(DateTime.now().year,
                                      DateTime.now().month, DateTime.now().day);
                                });
                              }
                              Navigator.of(context).pop();
                            },
                          ),
                          FlatButton(
                            child: Text(
                              AppLocalizations.of(context).translate(
                                'cancel',
                              ),
                              style: TextStyle(color: Colors.red),
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      ),
                    )
                  ],
                ));
          });
    }

    void _presentTimePicker() {
      showTimePicker(context: context, initialTime: TimeOfDay.now())
          .then((chosenTime) {
        if (chosenTime == null) {
          return;
        }
        selectedTime = chosenTime;
      });
    }

    void _presentTimePickerIOS() {
      showCupertinoModalPopup(
          context: context,
          builder: (BuildContext ctx) {
            return Container(
                height: MediaQuery.of(context).copyWith().size.height / 3,
                color: Colors.white,
                child: Column(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).copyWith().size.height *
                          4 /
                          15,
                      child: CupertinoDatePicker(
                        onDateTimeChanged: (DateTime newdate) {
                          setState(() {
                            selectedTime = TimeOfDay(
                                hour: newdate.hour, minute: newdate.minute);
                          });
                        },
                        initialDateTime: DateTime.now(),
                        use24hFormat: true,
                        minuteInterval: 1,
                        mode: CupertinoDatePickerMode.time,
                      ),
                    ),
                    Container(
                      height:
                          MediaQuery.of(context).copyWith().size.height / 15,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          FlatButton(
                            child: Text(
                              AppLocalizations.of(context).translate('submit'),
                              style: TextStyle(color: Colors.green),
                            ),
                            onPressed: () {
                              if (selectedTime == null) {
                                setState(() {
                                  selectedTime = TimeOfDay(
                                      hour: DateTime.now().hour,
                                      minute: DateTime.now().minute);
                                });
                                Navigator.of(context).pop();
                              }
                            },
                          ),
                          FlatButton(
                            child: Text(
                              AppLocalizations.of(context).translate('cancel'),
                              style: TextStyle(color: Colors.red),
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          )
                        ],
                      ),
                    )
                  ],
                ));
          });
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).translate('inventory_entry')),
        backgroundColor: _color,
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(AppLocalizations.of(context)
                              .translate('ingredient')),
                          DropdownButton(
                            value: dropdownValueIngredient,
                            icon: Icon(Icons.arrow_downward),
                            iconSize: 24,
                            elevation: 16,
                            onChanged: (String newValue) {
                              setState(() {
                                dropdownValueIngredient = newValue;
                                _entryMap['ingredientId'] =
                                    _ids[_dropdownValues.indexOf(newValue)];
                              });
                            },
                            items: _dropdownValues
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                          ),
                          TextFormField(
                            keyboardType: TextInputType.number,
                            validator: (value) {
                              try {
                                int newVal = int.parse(value);
                                return null;
                              } catch (error) {
                                return AppLocalizations.of(context)
                                    .translate('input_english');
                              }
                            },
                            onSaved: (value) {
                              _entryMap['amount'] = value;
                            },
                            decoration: InputDecoration(
                              labelText: "${AppLocalizations.of(context)
                                  .translate('amount')} (${_ingredientsAll[_dropdownValues.indexOf(dropdownValueIngredient)].scale})",
                            ),
                          ),
                          TextFormField(
                            keyboardType: TextInputType.number,
                            validator: (value) {
                              try {
                                int newVal = int.parse(value);
                                return null;
                              } catch (error) {
                                return AppLocalizations.of(context)
                                    .translate('input_english');
                              }
                            },
                            onSaved: (value) {
                              _entryMap['cost'] = value;
                            },
                            decoration: InputDecoration(
                              labelText: AppLocalizations.of(context)
                                  .translate('cost'),
                            ),
                          ),
                          TextFormField(
                            keyboardType: TextInputType.multiline,
                            maxLines: 3,
                            onSaved: (value) {
                              _entryMap['description'] = value;
                            },
                            decoration: InputDecoration(
                              labelText: AppLocalizations.of(context)
                                  .translate('description'),
                            ),
                          ),
                          Container(
                            height: 70,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(selectedDate == null
                                    ? AppLocalizations.of(context)
                                        .translate('no_date_chosen')
                                    : DateFormat.yMMMd().format(selectedDate)),
                                FlatButton(
                                  textColor: _color,
                                  child: Text(AppLocalizations.of(context)
                                      .translate('choose_date')),
                                  onPressed: (){
                                    if(Platform.isIOS){
                                      _presentDatePickerIOS();
                                    }
                                    else{
                                      _presentDatePickerAndroid();
                                    }
                                  },
                                )
                              ],
                            ),
                          ),
                          Container(
                            height: 70,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(selectedTime == null
                                    ? AppLocalizations.of(context)
                                        .translate('no_time_chosen')
                                    : "${selectedTime.hour}: ${selectedTime.minute}"),
                                FlatButton(
                                  textColor: _color,
                                  child: Text(AppLocalizations.of(context)
                                      .translate('choose_time')),
                                  onPressed: () {
                                    if (Platform.isIOS) {
                                      _presentTimePickerIOS();
                                    } else {
                                      _presentTimePicker();
                                    }
                                  },
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    RaisedButton(
                      child: Text(
                          AppLocalizations.of(context).translate('submit')),
                      onPressed: _submit,
                    )
                  ],
                ),
              ),
            ),
    );
  }
}
