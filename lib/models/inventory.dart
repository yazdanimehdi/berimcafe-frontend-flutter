import 'package:flutter/foundation.dart';

class InventoryIngredient {
  final String id;
  final double amount;
  final String ingredientId;
  final String scale;
  final String categoryId;

  InventoryIngredient(
      {@required this.id, @required this.ingredientId, @required this.amount, @required this.scale, this.categoryId});
}

class InventoryEntry{
  final String id;
  final double amount;
  final double cost;
  final String ingredientId;
  final String description;
  final DateTime dateTime;

  InventoryEntry({@required this.id, @required this.amount, @required this.cost, @required this.ingredientId, @required this.description, @required this.dateTime});
}

class InventoryOut{
  final String id;
  final double amount;
  final String ingredientId;
  final String description;
  final DateTime dateTime;

  InventoryOut({@required this.id, @required this.amount, @required this.ingredientId, @required this.description, @required this.dateTime});
}