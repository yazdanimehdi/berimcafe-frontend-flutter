import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import '../../providers/customers.dart';
import '../../locale/app_localization.dart';
import '../../models/customer.dart';
import '../../models/http_exceptions.dart';
import 'package:flutter/material.dart';

class AddCustomerScreen extends StatefulWidget {
  static const routeName = '/add-customer';

  @override
  _AddCustomerScreenState createState() => _AddCustomerScreenState();
}

class _AddCustomerScreenState extends State<AddCustomerScreen> {
  final _formKey = GlobalKey<FormState>();
  var _isLoading = false;
  Map<String, Object> _customerData = {
    'name': '',
    'phone1': '',
    'phone2': '',
    'address': '',
    'email1': '',
    'email2': '',
    'extra_info': '',
    'notes': '',
  };

  @override
  Widget build(BuildContext context) {
    void _showErrorDialog(String message) {
      showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title:
              Text(AppLocalizations.of(context).translate('login_error_title')),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text(AppLocalizations.of(context).translate('ok')),
              onPressed: () {
                Navigator.of(ctx).pop();
              },
            )
          ],
        ),
      );
    }

    Future<void> _submit() async {
      if (_formKey.currentState.validate()) {
        _formKey.currentState.save();
        Customer _customer = Customer(
          id: DateTime.now().toString(),
          name: _customerData['name'],
          phone1: _customerData['phone1'],
          phone2: _customerData['phone2'],
          email1: _customerData['email1'],
          email2: _customerData['email2'],
          address: _customerData['address'],
          extraInfo: _customerData['extra_info'],
          notes: _customerData['notes'],
        );
        setState(() {
          _isLoading = true;
        });
        try {
          await Provider.of<Customers>(context, listen: false)
              .newCustomerAdd(_customer);
        } on HttpException catch (error) {
          var errorMessage =
              AppLocalizations.of(context).translate('connection_error');
          _showErrorDialog(errorMessage);
        }
        setState(() {
          _isLoading = false;
          Navigator.of(context, rootNavigator: true)..pop();
        });
      }
    }

    return Scaffold(
      appBar: AppBar(
        title:
            Text(AppLocalizations.of(context).translate('add_customer_title')),
        backgroundColor: Colors.redAccent,
        actions: <Widget>[
          FlatButton(
            child: Text(AppLocalizations.of(context).translate('submit'), style: TextStyle(color: Colors.white),),
            hoverColor: Colors.white,
            textColor: Colors.white,
            onPressed: _submit,
          )
          ],
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(30),
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: TextFormField(
                          onSaved: (value) {
                            _customerData['name'] = value;
                          },
                          decoration: InputDecoration(
                              fillColor: Colors.transparent,
                              filled: true,
                              labelText:
                                  AppLocalizations.of(context).translate('name')),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: TextFormField(
                          onSaved: (value) {
                            _customerData['phone1'] = value;
                          },
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              fillColor: Colors.transparent,
                              filled: true,
                              labelText: AppLocalizations.of(context)
                                  .translate('phone1')),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: TextFormField(
                          onSaved: (value) {
                            _customerData['phone2'] = value;
                          },
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              fillColor: Colors.transparent,
                              filled: true,
                              labelText: AppLocalizations.of(context)
                                  .translate('phone2')),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: TextFormField(
                          onSaved: (value) {
                            _customerData['email1'] = value;
                          },
                          decoration: InputDecoration(
                              fillColor: Colors.transparent,
                              filled: true,
                              labelText: AppLocalizations.of(context)
                                  .translate('email1')),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: TextFormField(
                          onSaved: (value) {
                            _customerData['email2'] = value;
                          },
                          decoration: InputDecoration(
                              fillColor: Colors.transparent,
                              filled: true,
                              labelText: AppLocalizations.of(context)
                                  .translate('email2')),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: TextFormField(
                          onSaved: (value) {
                            _customerData['address'] = value;
                          },
                          decoration: InputDecoration(
                              fillColor: Colors.transparent,
                              filled: true,
                              labelText: AppLocalizations.of(context)
                                  .translate('address')),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: TextFormField(
                          onSaved: (value) {
                            _customerData['extra_info'] = value;
                          },
                          decoration: InputDecoration(
                              fillColor: Colors.transparent,
                              filled: true,
                              labelText: AppLocalizations.of(context)
                                  .translate('extra_info')),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: TextFormField(
                          onSaved: (value) {
                            _customerData['notes'] = value;
                          },
                          decoration: InputDecoration(
                              fillColor: Colors.transparent,
                              filled: true,
                              labelText: AppLocalizations.of(context)
                                  .translate('notes')),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          child: Text(
                              AppLocalizations.of(context).translate('submit')),
                          onPressed: _submit,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
