import 'package:cafe_management_app/providers/foods.dart';

import '../graphql_operation/mutations/mutations.dart';
import 'package:flutter/foundation.dart';

import '../models/http_exceptions.dart';

import '../graphql_operation/queries/queries.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import '../services/graphQLConf.dart';
import 'package:flutter/material.dart';
import '../providers/foods_all.dart';
import '../providers/foods.dart';


class IngredientsProvider with ChangeNotifier {
  final String authToken;
  List<Map<String, Object>> _ingredientList;

  IngredientsProvider(this.authToken, this._ingredientList);

  List<Map<String, Object>> get getIngredients {
    return [..._ingredientList];
  }

  Ingredients getIngredientsById(String id) {
    Ingredients _ingredient;
    _ingredientList.forEach((element) {
      List _ingredientsListEach = element['ingredinets'];
      _ingredientsListEach.forEach((el) {
        if (el.id == id) {
          _ingredient = el;
        }
      });
    });
    return _ingredient;
  }

  Map<String, List<String>> get getCategories{
    List<String>  finalList = [];
    List<String> finalListId = [];
    _ingredientList.forEach((element) {
      IngredientsCategory _ingredient = element['category'];
      finalList.add(_ingredient.name);
      finalListId.add(_ingredient.id);
    });
    return {'names': finalList, 'ids': finalListId};
  }

  IngredientsCategory getCategoriesById(String id){
    IngredientsCategory _myCat;
    _ingredientList.forEach((element) {
      IngredientsCategory _ingredient = element['category'];
      if(_ingredient.id == id){
        _myCat = _ingredient;
      }
    });
    return _myCat;
  }

  Map<String, List<String>> get  getAllCategories{
    List<String> _categoryNameList = [];
    List<String> _categoryIds = [];
    _ingredientList.forEach((inst){
      IngredientsCategory _cat = inst['category'];
      _categoryNameList.add(_cat.name);
      _categoryIds.add(_cat.id);
    });
    return {'names': _categoryNameList, 'ids': _categoryIds};
  }

  Map<String, List<Object>> getIngredientsByCategory(String id) {
    List<String>  finalList = [];
    List<String> finalListId = [];
    List<Ingredients> finalListAll = [];
    _ingredientList.forEach((element) {
      IngredientsCategory _cat = element['category'];
      if (_cat.id == id){
        List<Ingredients> _myList = element['ingredinets'];
        _myList.forEach((el){
          finalList.add(el.name);
          finalListId.add(el.id);
          finalListAll.add(el);
        });
      }
    });
    return {'names': finalList, 'ids': finalListId, 'all':finalListAll};
  }

  Future<void> getAllIngredients() async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    List<Map<String, Object>> _loadedIngredients = [];
    try {
      QueryResult result = await _client.query(
        QueryOptions(documentNode: gql(QueryAll().getIngredientsCategory())),
      );
      if (!result.hasException) {
        for (var i = 0; i < result.data['allIngredientsCategory'].length; i++) {
          final IngredientsCategory _ingredientCategory = IngredientsCategory(
              result.data['allIngredientsCategory'][i]['id'],
              result.data['allIngredientsCategory'][i]['name']);
          final List<Ingredients> _ingredientList = [];
          for (var j = 0;
              j <
                  result.data['allIngredientsCategory'][i]['ingredientsSet']
                      .length;
              j++) {
            final Ingredients _ingredient = Ingredients(
                id: result.data['allIngredientsCategory'][i]['ingredientsSet']
                    [j]['id'],
                name: result.data['allIngredientsCategory'][i]['ingredientsSet']
                    [j]['name'],
                scale: result.data['allIngredientsCategory'][i]
                    ['ingredientsSet'][j]['scale'],
                price: result.data['allIngredientsCategory'][i]
                    ['ingredientsSet'][j]['price'],
                description: result.data['allIngredientsCategory'][i]
                    ['ingredientsSet'][j]['description']);
            _ingredientList.add(_ingredient);
          }
          _loadedIngredients.add({
            'category': _ingredientCategory,
            'ingredinets': _ingredientList
          });
        }
      } else {
        throw HttpException(result.exception.toString());
      }
      QueryResult resultUndefined = await _client.query(
        QueryOptions(
            documentNode: gql(QueryAll().getIngredientsWithoutCategory())),
      );
      if (!resultUndefined.hasException) {
        IngredientsCategory _categoryUndefined =
            IngredientsCategory('-1', 'Undefined');
        final List<Ingredients> _ingredientsList = [];
        for (var k = 0;
            k < resultUndefined.data['allIngredientsWithoutCategory'].length;
            k++) {
          Ingredients _ingredient = Ingredients(
              id: resultUndefined.data['allIngredientsWithoutCategory'][k]
                  ['id'],
              name: resultUndefined.data['allIngredientsWithoutCategory'][k]
                  ['name'],
              price: resultUndefined.data['allIngredientsWithoutCategory'][k]
                  ['price'],
              description: resultUndefined.data['allFoodWithoutCategory'][k]
                  ['description'],
              scale: resultUndefined.data['allFoodWithoutCategory'][k]
                  ['scale']);
          _ingredientsList.add(_ingredient);
        }
        if (resultUndefined.data['allIngredientsWithoutCategory'].length > 0) {
          _loadedIngredients.add({
            'category': _categoryUndefined,
            'ingredients': _ingredientsList
          });
        }
      }
      _ingredientList = _loadedIngredients;
      notifyListeners();
    } catch (error) {
      print(error.toString());
      throw error;
    }
  }

  Future<void> addIngredientCategory(String name) async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult result = await _client.mutate(
        MutationOptions(
            documentNode: gql(MutationsAll().addIngredientCategory(name))),
      );
      if (!result.hasException) {
        getAllIngredients();
      } else {
        throw HttpException(result.exception.toString());
      }
    } catch (error) {
      throw error;
    }
  }

  Future<void> editIngredientCategory(String id, String name) async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult result = await _client.mutate(
        MutationOptions(
            documentNode:
                gql(MutationsAll().editIngredientsCategory(id, name))),
      );
      if (!result.hasException) {
        getAllIngredients();
      } else {
        throw HttpException(result.exception.toString());
      }
    } catch (error) {
      throw error;
    }
  }

  Future<void> deleteIngredientCategory(String id) async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult result = await _client.mutate(
        MutationOptions(
            documentNode: gql(MutationsAll().deleteIngredientsCategory(id))),
      );
      if (!result.hasException) {
        getAllIngredients();
      } else {
        throw HttpException(result.exception.toString());
      }
    } catch (error) {
      throw error;
    }
  }

  Future<void> addIngredient(Map<String, String> data) async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult result = await _client.mutate(
        MutationOptions(documentNode: gql(MutationsAll().addIngredient(data))),
      );
      if (!result.hasException) {
        getAllIngredients();
      } else {
        throw HttpException(result.exception.toString());
      }
    } catch (error) {
      throw error;
    }
  }

  Future<void> deleteIngredient(String id) async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult result = await _client.mutate(
        MutationOptions(documentNode: gql(MutationsAll().deleteIngredient(id))),
      );
      if (!result.hasException) {
        getAllIngredients();
      } else {
        throw HttpException(result.exception.toString());
      }
    } catch (error) {
      throw error;
    }
  }

  Future<void> editIngredient(String id, String name, String price, String scale, String catId, String description) async{
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult result = await _client.mutate(
        MutationOptions(documentNode: gql(MutationsAll().editIngredient(id, name, price, scale, catId, description))),
      );
      if (!result.hasException) {
        getAllIngredients();
      } else {
        throw HttpException(result.exception.toString());
      }
    } catch (error) {
      throw error;
    }
  }

}
