import 'package:flutter/material.dart';


class Category {
  final int id;
  final String name;
  final String english;
  final Color color;
  final IconData icon;

  const Category({@required this.id, @required this.name, @required this.color, @required this.icon, @required this.english});
}