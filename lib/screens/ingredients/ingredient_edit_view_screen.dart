import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/ingredients.dart';
import '../../locale/app_localization.dart';
import '../../models/http_exceptions.dart';

class IngredientEditViewScreen extends StatefulWidget {
  static const routeName = '/ingredient-view';

  @override
  _IngredientEditViewScreenState createState() =>
      _IngredientEditViewScreenState();
}

class _IngredientEditViewScreenState extends State<IngredientEditViewScreen> {
  String name;
  String price;
  String scale;
  String catId;
  String description;
  String dropdownCategoryValue;
  var _isEditing = false;
  var _isLoading = false;
  var _isInit = true;

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> _formKey = GlobalKey();
    final arguments = ModalRoute.of(context).settings.arguments as Map;
    final ingredientId = arguments['ingredientId'];
    final MaterialColor _color = arguments['color'];
    final _ingredient = Provider.of<IngredientsProvider>(context)
        .getIngredientsById(ingredientId);
    final _categories =
        Provider.of<IngredientsProvider>(context).getAllCategories;
    final _categoryNames = _categories['names'];
    final _categoryIds = _categories['ids'];
    final _categoryName = arguments['categoryName'];
    if (_isInit) {
      dropdownCategoryValue = _categoryName;
      name = _ingredient.name;
      price = _ingredient.price.toString();
      description = _ingredient.description;
      catId = arguments['categoryId'];
      scale = _ingredient.scale;
      _isInit = false;
    }
    void _showErrorDialog(String message) {
      showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title:
              Text(AppLocalizations.of(context).translate('login_error_title')),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(ctx).pop();
              },
            )
          ],
        ),
      );
    }

    Future<void> _submit(String ingredientId) async {
      if (!_formKey.currentState.validate()) {
        // Invalid!
        return;
      }
      _formKey.currentState.save();
      setState(() {
        _isLoading = true;
      });
      try {
        await Provider.of<IngredientsProvider>(context, listen: false)
            .editIngredient(
                ingredientId, name, price, scale, catId, description);
      } on HttpException catch (error) {
        print(error.toString());
        var errorMessage = 'Editing failed';
        if (error.toString().contains('Please, enter valid credentials')) {
          errorMessage =
              AppLocalizations.of(context).translate('login_error_message');
        }
        _showErrorDialog(errorMessage);
      } catch (error) {
        print(error);
        var errorMessage =
            AppLocalizations.of(context).translate('connection_error');
        _showErrorDialog(errorMessage);
      }

      setState(() {
        _isLoading = false;
      });
      Navigator.of(context, rootNavigator: true)..pop();
    }

    return Scaffold(
        appBar: AppBar(
          title: Text(_ingredient.name),
          backgroundColor: _color,
          actions: <Widget>[
            FlatButton(
              child: Text(
                AppLocalizations.of(context).translate('edit'),
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                setState(() {
                  _isEditing = !_isEditing;
                });
              },
            )
          ],
        ),
        body: _isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : _isEditing
                ? Form(
                    key: _formKey,
                    child: Padding(
                        padding: EdgeInsets.all(20),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  CircleAvatar(
                                    child: Text(
                                      _ingredient.name[0],
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30),
                                    ),
                                    backgroundColor: _color,
                                    maxRadius: 50,
                                    minRadius: 20,
                                  ),
                                  Container(
                                    width: 200,
                                    child: TextFormField(
                                      initialValue: _ingredient.name,
                                      decoration: InputDecoration(
                                          labelText:
                                              AppLocalizations.of(context)
                                                  .translate('name')),
                                      onSaved: (value) {
                                        name = value;
                                      },
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Expanded(
                                  child: Container(
                                      child: ListView(children: <Widget>[
                                Text(AppLocalizations.of(context)
                                    .translate('category')),
                                DropdownButton(
                                  value: dropdownCategoryValue,
                                  icon: Icon(Icons.arrow_downward),
                                  iconSize: 24,
                                  elevation: 16,
                                  onChanged: _categoryNames == null
                                      ? (_) {}
                                      : (String newValue) {
                                          int _index =
                                              _categoryNames.indexOf(newValue);

                                          setState(() {
                                            dropdownCategoryValue = newValue;
                                            catId = _categoryIds[_index];
                                          });
                                        },
                                  items: _categoryNames == null
                                      ? null
                                      : _categoryNames
                                          .map<DropdownMenuItem<String>>(
                                              (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(value),
                                          );
                                        }).toList(),
                                ),
                                TextFormField(
                                  decoration: InputDecoration(
                                    labelText: AppLocalizations.of(context)
                                        .translate('price'),
                                  ),
                                  onSaved: (value) {
                                    price = value;
                                  },
                                  initialValue: _ingredient.price.toString(),
                                  keyboardType: TextInputType.number,
                                  textInputAction: TextInputAction.done,
                                ),
                                TextFormField(
                                  decoration: InputDecoration(
                                      labelText: AppLocalizations.of(context)
                                          .translate('scale')),
                                  onSaved: (value) {
                                    scale = value;
                                  },
                                  initialValue: _ingredient.scale,
                                ),
                                TextFormField(
                                  decoration: InputDecoration(
                                      labelText: AppLocalizations.of(context)
                                          .translate('description')),
                                  onSaved: (value) {
                                    description = value;
                                  },
                                  initialValue: _ingredient.description,
                                  maxLines: 3,
                                  keyboardType: TextInputType.multiline,
                                ),
                                RaisedButton(
                                  child: Text(AppLocalizations.of(context)
                                      .translate('submit')),
                                  onPressed: () => _submit(_ingredient.id),
                                )
                              ])))
                            ])))
                : Padding(
                    padding: EdgeInsets.all(20),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              CircleAvatar(
                                child: Text(
                                  _ingredient.name[0],
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 30),
                                ),
                                backgroundColor: _color,
                                maxRadius: 50,
                                minRadius: 20,
                              ),
                              Container(
                                width: 200,
                                child: Text(
                                  _ingredient.name,
                                  style: TextStyle(
                                      fontSize: 28,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.start,
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Expanded(
                              child: Container(
                                  child: ListView(children: <Widget>[
                            ListTile(
                              title: Text(AppLocalizations.of(context)
                                  .translate(('category'))),
                              trailing: Text(_categoryName),
                            ),
                            ListTile(
                              title: Text(AppLocalizations.of(context)
                                  .translate('price')),
                              trailing: Text(_ingredient.price.toString()),
                            ),
                            ListTile(
                              title: Text(AppLocalizations.of(context)
                                  .translate(('scale'))),
                              trailing: Text(_ingredient.scale),
                            ),
                            ListTile(
                              title: Text(AppLocalizations.of(context)
                                  .translate(('description'))),
                              trailing: Text(_ingredient.description == null ? "": _ingredient.description),
                            ),
                          ])))
                        ])));
  }
}
