import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/personnel.dart';
import '../../locale/app_localization.dart';
import '../../models/personnel.dart';
import '../../providers/chat.dart';

class PersonnelListScreen extends StatefulWidget {
  static const routeName = '/personnel-screen';

  @override
  _PersonnelListScreenState createState() => _PersonnelListScreenState();
}

class _PersonnelListScreenState extends State<PersonnelListScreen> {
  var _isInit = true;
  var _isLoading = false;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<PersonnelProvider>(context).getAndFetchPersonnel().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    List<Personnel> _personnelList = Provider.of<PersonnelProvider>(context).getPersonnelList;
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).translate('personnel')),
        backgroundColor: Colors.deepOrange,
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: const EdgeInsets.all(20.0),
              child: ListView.separated(
                  itemBuilder: (BuildContext ctx, int index) {
                    return ListTile(
                      isThreeLine: true,
                      title: Text(_personnelList[index].userName, style: TextStyle(fontWeight: FontWeight.bold),),
                      subtitle: Text("${_personnelList[index].firstName} ${_personnelList[index].lastName}"),
                      trailing: Container(
                        child: Column(
                          children: <Widget>[
                            for ( var i in _personnelList[index].rolesList ) Text(i.name)
                          ],
                        ),
                      ),

                    );
                  },
                  separatorBuilder: (context, index) => Divider(
                        height: 5,
                        color: Colors.black26,
                      ),
                  itemCount: _personnelList.length),
            ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.deepOrange,
        child: Icon(Icons.add, color: Colors.white,),
        onPressed: (){

        },
      ),
    );
  }
}
