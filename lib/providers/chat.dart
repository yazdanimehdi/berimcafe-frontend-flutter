import 'package:flutter/foundation.dart';
import 'dart:async';
import 'dart:io';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../services/graphQLConf.dart';
import '../graphql_operation/subscriptions/subscriptions.dart';
import '../models/chat_models.dart';
import '../graphql_operation/queries/queries.dart';
import '../graphql_operation/mutations/mutations.dart';
import '../models/http_exceptions.dart';

class ChatProvider with ChangeNotifier {
  String authToken;
  List<Chat> _chatsList = [];
  List<ChatUser> _chatUsers = [];
  bool _chatSetDone = false;

  List<Chat> get getChatList {
    return [..._chatsList];
  }

  List<ChatUser> get getChatUsers {
    return [..._chatUsers];
  }

  void setAuth(String token) {
    authToken = token;
  }

  int newMessages(String chatId) {
    int newMessage = 0;
    _chatsList.forEach((el) {
      if (el.id == chatId) {
        el.messages.forEach((message) {
          if (!message.read) {
            newMessage++;
          }
        });
      }
    });
    return newMessage;
  }

  ChatUser getChatUser(String id) {
    ChatUser _result;
    _chatUsers.forEach((el) {
      if (el.id == id) {
        _result = el;
      }
    });
    return _result;
  }

  Chat getChatById(String id) {
    return _chatsList.firstWhere((el) {
      if (el.id == id) {
        return true;
      } else {
        return false;
      }
    });
  }

  bool get authSetDone {
    if (authToken != null) {
      return true;
    } else {
      return false;
    }
  }

  bool get chatSetDone {
    return _chatSetDone;
  }

  Future<void> getAndSetChats() async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    QueryResult result = await _client.query(
      QueryOptions(documentNode: gql(QueryAll().getAllChats())),
    );
    final List<Chat> _loadedChats = [];
    try {
      if (!result.hasException) {
        List<ChatUser> _loadedUsers = [];
        for (var i = 0; i < result.data['allChats'].length; i++) {
          List<ChatUser> usersList = [];
          for (var j = 0; j < result.data['allChats'][i]['users'].length; j++) {
            LazyCacheMap cacheMap = result.data['allChats'][i]['users'][j];
            List<String> roleNames = [];
            for (var k = 0;
                k <
                    result.data['allChats'][i]['users'][j]['rolepersonnelSet']
                        .length;
                k++) {
              roleNames.add(result.data['allChats'][i]['users'][j]
                  ['rolepersonnelSet'][k]['role']['name']);
            }
            ChatUser user = ChatUser(
                id: cacheMap['id'],
                firstName: cacheMap['firstName'],
                lastName: cacheMap['lastName'],
                userName: cacheMap['username'],
                roleNames: roleNames);
            if (!_loadedUsers.contains(user)) {
              _loadedUsers.add(user);
            }
            usersList.add(user);
          }
          List<ChatMessages> _chatMessages = [];
          for (var j = 0;
              j < result.data['allChats'][i]['chatmessagesSet'].length;
              j++) {
            LazyCacheMap cacheMap =
                result.data['allChats'][i]['chatmessagesSet'][j];
            ChatMessages message = ChatMessages(
                id: cacheMap['id'],
                dateTime: DateTime.parse(cacheMap['dateTime']),
                fromId: result.data['allChats'][i]['chatmessagesSet'][j]
                    ['fromUser']['id'],
                message: cacheMap['message'],
                read: usersList[0].id ==
                        result.data['allChats'][i]['chatmessagesSet'][j]
                            ['fromUser']['id']
                    ? true
                    : cacheMap['read'],
                repliedToId: result.data['allChats'][i]['chatmessagesSet'][j]
                            ['replyMessage'] !=
                        null
                    ? result.data['allChats'][i]['chatmessagesSet'][j]
                        ['replyMessage']['id']
                    : null,
                loading: false);
            _chatMessages.add(message);
          }
          Chat _chat = Chat(
              id: result.data['allChats'][i]['id'],
              dateTime: result.data['allChats'][i]['dateTime'] != null
                  ? DateTime.parse(result.data['allChats'][i]['dateTime'])
                  : null,
              users: usersList,
              messages: _chatMessages,
              name: result.data['allChats'][i]['name']);
          _loadedChats.add(_chat);
        }
        _chatsList = _loadedChats;
        _chatUsers = _loadedUsers;
      } else {
        throw (HttpException(result.exception.toString()));
      }
    } catch (error) {
      print(error);
      throw (error);
    }
  }

  Future<void> subscribeChat() async {
    await getAndSetChats();
    GraphQLClient _client = GraphQLConfiguration().wsClient();
    _chatSetDone = true;
    try {
      var data = _client.subscribe(Operation(
        operationName: "chatMessages",
        documentNode: gql(SubscriptionsAll().getMessages(authToken)),
      ));
      data.listen((onData) {
        var chatId = onData.data['chatMessagesCreatedUpdated']['chat']['id'];
        var messageId = onData.data['chatMessagesCreatedUpdated']['id'];
        var fromID =
            onData.data['chatMessagesCreatedUpdated']['fromUser']['id'];
        if (_chatsList
                .firstWhere((chat) {
                  if (chat.id == chatId) {
                    return true;
                  } else {
                    return false;
                  }
                })
                .users[0]
                .id ==
            fromID) {
        } else {
          _chatsList
              .firstWhere((el) {
                if (el.id == chatId) {
                  return true;
                } else {
                  return false;
                }
              })
              .messages
              .add(ChatMessages(
                  read: onData.data['chatMessagesCreatedUpdated']['read'],
                  dateTime: DateTime.parse(
                      onData.data['chatMessagesCreatedUpdated']['dateTime']),
                  repliedToId: onData.data['chatMessagesCreatedUpdated']
                              ['replyMessage'] ==
                          null
                      ? null
                      : onData.data['chatMessagesCreatedUpdated']
                          ['replyMessage']['id'],
                  message: onData.data['chatMessagesCreatedUpdated']['message'],
                  fromId: onData.data['chatMessagesCreatedUpdated']['fromUser']
                      ['id'],
                  id: onData.data['chatMessagesCreatedUpdated']['id'],
                  loading: false));
          notifyListeners();
        }
      });
    } catch (error) {
      throw (error);
    }
  }

  Future<void> addChat(List<int> userIds, String name) async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult result = await _client.mutate(
        MutationOptions(
            documentNode: gql(MutationsAll().createChatSession(userIds, name))),
      );
      if (result.hasException) {
        throw (HttpException(result.exception.toString()));
      }
    } catch (error) {
      print(error);
      throw (error);
    }
  }

  Future<void> sendReadMessage(String messageId) async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult result = await _client.mutate(
        MutationOptions(
            documentNode: gql(MutationsAll().sendReadMessage(messageId))),
      );

      if (result.hasException) {
        throw (HttpException(result.exception.toString()));
      } else {
        if (!result.data['readChatMessage']['status']) {
          throw (HttpException('Message Does Not Exist'));
        }
      }
    } catch (error) {
      print(error);
      throw (error);
    }
  }

  Future<void> sendMessage(
      String chatId, String message, String replyId) async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult result = await _client.mutate(MutationOptions(
          documentNode:
              gql(MutationsAll().sendMessage(chatId, message, replyId))));
      Chat _chat = _chatsList.firstWhere((el) {
        if (el.id == chatId) {
          return true;
        } else {
          return false;
        }
      });
      _chat.messages.add(ChatMessages(
          loading: true,
          read: true,
          repliedToId: replyId,
          message: message,
          dateTime: DateTime.now(),
          fromId: _chat.users[0].id));
      notifyListeners();
      if (!result.hasException) {
        if (!result.data['createChatMessage']['status']) {
          throw (HttpException(result.data['createChatMessage']['message']));
        } else {
          _chat.messages[_chat.messages.length - 1].loading = false;
          _chat.messages[_chat.messages.length - 1].id =
              result.data['createChatMessage']['messages']['id'];
          notifyListeners();
        }
      } else {
        throw (HttpException(result.exception.toString()));
      }
    } catch (error) {
      print(error);
      throw (error);
    }
  }

  void addChatMessage(ChatMessages message, String chatId) {
    _chatsList
        .firstWhere((el) {
          if (el.id == chatId) {
            return true;
          } else {
            return false;
          }
        })
        .messages
        .add(message);

    notifyListeners();
  }
}
