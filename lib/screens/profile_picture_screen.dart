import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ProfilePictureScreen extends StatelessWidget {
  static const routeName = '/profile-image';
  @override
  Widget build(BuildContext context) {
    final String imageUrl = ModalRoute.of(context).settings.arguments as String;

    return Scaffold(
      appBar: AppBar(
        title: Text('ویرایش عکس پروفایل'),
        elevation: 0,
        backgroundColor: Colors.black,
      ),
      body: Container(
        color: Colors.black,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            SizedBox(height: 0,),
            CachedNetworkImage(
              imageUrl: imageUrl,
              placeholder: (context, url) => CircularProgressIndicator(),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.add_a_photo, color: Colors.white,),
                  onPressed: (){},
                ),
                IconButton(
                  icon: Icon(Icons.delete, color: Colors.white,),
                  onPressed: (){},
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
