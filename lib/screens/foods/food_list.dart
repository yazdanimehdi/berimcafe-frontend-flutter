import 'package:cafe_management_app/providers/ingredients.dart';

import '../../screens/foods/food_add_screen.dart';

import '../../locale/app_localization.dart';

import '../../providers/locale.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/foods.dart';
import '../../providers/foods_all.dart';
import 'food_edit_view_screen.dart';

class FoodScreen extends StatefulWidget {
  static final routName = '/menu-item';

  @override
  _FoodScreenState createState() => _FoodScreenState();
}


class _FoodScreenState extends State<FoodScreen> {
  var _isInit = true;
  var _isLoading = false;

  @override
  Widget build(BuildContext context) {
    Map<String, Object> arguments = ModalRoute.of(context).settings.arguments as Map;
    final categoryIndex = arguments['index'];
    final List _categoryList = Provider.of<FoodsAll>(context).getCategories[categoryIndex]['foods'];
    final FoodCategory _category = Provider.of<FoodsAll>(context).getCategories[categoryIndex]['category'];
    return Scaffold(
      appBar: AppBar(
        backgroundColor: arguments['color'],
        title: Text(_category.name),
      ),
      body: ListView.separated(
          itemBuilder: (BuildContext ctx, int index) => Dismissible(
            direction: Provider.of<LocaleProvider>(context).locale == Locale('en', 'US') ? DismissDirection.endToStart: DismissDirection.startToEnd,
            onDismissed: (direction) => Provider.of<FoodsAll>(context, listen: false).deleteFood(_categoryList[index]['food'].id),
            key: UniqueKey(),
            background: Container(
              height: 80,
              color:Theme.of(context).errorColor,
              child: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Icon(Icons.delete, color: Colors.white, size: 28,),
                    SizedBox(height: 2,),
                    Text(AppLocalizations.of(context).translate('delete'), style: TextStyle(color: Colors.white),)
                  ],
                ),
              ),
              alignment: Alignment.centerRight,
              padding: EdgeInsets.only(right: 20),
            ),
            child: ListTile(
              onTap: (){
                Navigator.of(context).pushNamed(FoodViewEditScreen.routeName, arguments:{'category':categoryIndex, 'index':index, 'color':arguments['color']} );
              },
              title: Text(_categoryList[index]['food'].name),
              subtitle: Text(_categoryList[index]['food'].description),
              trailing: Column(
                children: <Widget>[
                  Text(_categoryList[index]['food'].price.toString()),
                  _categoryList[index]['food'].status ? Text('فعال', style: TextStyle(color: Colors.green),): Text('غیرفعال', style: TextStyle(color: Colors.red),)
                ],
              )
            ),
          ),
          separatorBuilder:  (context, index) => Divider(
            height: 20,
            color: Colors.black26,
          ),
          itemCount: _categoryList.length),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add, color: Colors.white,),
        backgroundColor: arguments['color'],
        onPressed: (){
          Navigator.of(context).pushNamed(FoodAddScreen.routeName, arguments: {'category':_category.id, 'color':arguments['color']});
        },
      ),
    );
  }
}
