import '../locale/app_localization.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import './profile_picture_screen.dart';
import '../providers/auth.dart';

class ProfileScreen extends StatelessWidget {
  final String imageUrl =
      'https://avatars0.githubusercontent.com/u/8264639?s=460&v=4';

  void selectProfilePicture(BuildContext context) {
    Navigator.of(context)
        .pushNamed(ProfilePictureScreen.routeName, arguments: imageUrl);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 45,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(const Radius.circular(10)),
              ),
              child: Card(
                color: Theme.of(context).accentColor,
                elevation: 10,
                child: Center(
                    child: Text('کافه دیسفان',
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white))),
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Stack(
                  alignment: Alignment.bottomCenter,
                  children: <Widget>[
                    CircleAvatar(
                        backgroundColor: Colors.redAccent,
                        radius: 50,
                        backgroundImage: NetworkImage(imageUrl)),
                    InkWell(
                      splashColor: Colors.red,
                      onTap: () => selectProfilePicture(context),
                      child: Container(
                        height: 50,
                        width: 100,
                        decoration: BoxDecoration(
                            color: Colors.black54,
                            borderRadius: BorderRadius.only(
                                bottomLeft: const Radius.circular(50),
                                bottomRight: const Radius.circular(50))),
                        child: Card(
                          elevation: 0,
                          color: Colors.transparent,
                          child: Center(
                              child: Text(
                            AppLocalizations.of(context).translate('edit'),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.bold),
                          )),
                        ),
                      ),
                    )
                  ],
                ),
                Container(
                  width: 200,
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Mehdi Yazdani',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 28),
                      ),
                      Container(
                          width: double.infinity,
                          child: Card(
                              color: Colors.transparent,
                              elevation: 0,
                              child: Center(
                                  child: Text(
                                '${AppLocalizations.of(context).translate('role')}: برنامه‌نویس',
                                style: TextStyle(
                                    fontSize: 22, fontWeight: FontWeight.bold),
                              )))),
                    ],
                  ),
                )
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Expanded(
              child: ListView(

                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.account_box),
                    title: Text(AppLocalizations.of(context).translate('edit_personal_info')),
                    trailing: Icon(Icons.arrow_forward_ios),
                    onTap: (){},
                  ),
                  ListTile(
                    leading: Icon(Icons.lock_open),
                    title: Text(AppLocalizations.of(context).translate('edit_login_info')),
                    trailing: Icon(Icons.arrow_forward_ios),
                    onTap: (){},
                  ),
                  ListTile(
                    leading: Icon(Icons.vpn_key),
                    title: Text(AppLocalizations.of(context).translate('view_permissions')),
                    trailing: Icon(Icons.arrow_forward_ios),
                    onTap: (){},
                  )
                ],
              ),
            ),
            Center(
              child: RaisedButton(
                onPressed: (){
                  Provider.of<Auth>(context, listen: false).logout();
                },
                color: Theme.of(context).errorColor,
                child: Text(AppLocalizations.of(context).translate('logout'), style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),),
              ),
            )
          ],
        ),
      );
  }
}
