
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class LocaleProvider with ChangeNotifier{
  Locale locale;


  bool get isSet {
    return locale != null;
  }


  Future <void> changeLocale(Locale changedLocale) async{
    locale = changedLocale;
    final prefs = await SharedPreferences.getInstance();
    Map<String, String> localeData = {
      "countryCode": locale.countryCode,
      "languageCode": locale.languageCode,
    };
    var dumpData = json.encode(localeData);
    prefs.setString('locale', dumpData);
    notifyListeners();
  }

  Future<bool> tryChangeLocale() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('locale')) {
      return false;
    }
    final extractedLocaleData =
    json.decode(prefs.getString('locale')) as Map<String, Object>;
    locale = Locale(extractedLocaleData['languageCode'], extractedLocaleData['countryCode']);
    notifyListeners();
    return true;
  }


}