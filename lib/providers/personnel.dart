import '../models/personnel.dart';
import '../graphql_operation/queries/queries.dart';
import '../services/graphQLConf.dart';
import '../models/http_exceptions.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter/foundation.dart';

class PersonnelProvider with ChangeNotifier {
  final String authToken;
  List<Personnel> _personnelList;

  PersonnelProvider(this.authToken, this._personnelList);

  List<Personnel> get getPersonnelList {
    return [..._personnelList];
  }

  Future<void> getAndFetchPersonnel() async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    final List<Personnel> _loadedPersonnel = [];
    try {
      QueryResult result = await _client.query(
        QueryOptions(documentNode: gql(QueryAll().getPersonnelAll())),
      );
      if (!result.hasException) {
        for (var i = 0; i < result.data['allPersonnel'].length; i++) {
          List<Roles> _roles = [];
          for (var j = 0;
          j < result.data['allPersonnel'][i]['rolepersonnelSet'].length;
          j++) {
            LazyCacheMap _rolesCacheMap =
            result.data['allPersonnel'][i]['rolepersonnelSet'][j]['role'];
            List<Permission> _permissions = [];
            for (var k = 0;
            k <
                result
                    .data['allPersonnel'][i]['rolepersonnelSet'][j]['role']
                ['permissionsroleSet']
                    .length;
            k++) {
              LazyCacheMap _permissionsCacheMap = result.data['allPersonnel'][i]
              ['rolepersonnelSet'][j]['role']['permissionsroleSet'][k]['permission'];
              Permission _permission = Permission(_permissionsCacheMap['id'],
                  _permissionsCacheMap['permission']);
              _permissions.add(_permission);
            }
            Roles _role = Roles(
                id: _rolesCacheMap['id'],
                name: _rolesCacheMap['name'],
                roleRelated: _rolesCacheMap['roleRelated'],
                permissionList: _permissions);
            _roles.add(_role);
          }
          Personnel _personnel = Personnel(
              id: result.data['allPersonnel'][i]['id'],
              email: result.data['allPersonnel'][i]['email'],
              phone: result.data['allPersonnel'][i]['phone'],
              rolesList: _roles,
              userName: result.data['allPersonnel'][i]['username'],
              firstName: result.data['allPersonnel'][i]['firstName'],
              lastName: result.data['allPersonnel'][i]['lastName']);
          _loadedPersonnel.add(_personnel);
        }
        _personnelList = _loadedPersonnel;
        notifyListeners();
      } else {
        throw (HttpException(result.exception.toString()));
      }
    }
    catch(error){
      print(error);
      throw(error);
    }
  }
}
