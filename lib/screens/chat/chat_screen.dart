import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/chat.dart';
import '../../models/chat_models.dart';
import 'package:bubble/bubble.dart';

class ChatScreen extends StatefulWidget {
  static const routeName = 'chat-screen';

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  var _isInit = true;
  ScrollController _scrollController = ScrollController(
    initialScrollOffset: 0.0,
    keepScrollOffset: true,
  );
  @override
  void dispose() {
    _scrollController.dispose();
    _controller.dispose();
    super.dispose();
  }
  TextEditingController _controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final chatId = ModalRoute.of(context).settings.arguments as String;
    Chat _chat = Provider.of<ChatProvider>(context).getChatById(chatId);
    AppBar appbar = AppBar(
      title: Text(
          "${_chat.users[_chat.users.length - 1].firstName} ${_chat.users[_chat.users.length - 1].lastName}"),
      actions: <Widget>[CircleAvatar()],
    );
    return Scaffold(
      appBar: appbar,
      body: Container(
        color: Colors.blueGrey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                controller: _scrollController,
                reverse: true,
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: <Widget>[
                      for (var i in _chat.messages)
                        i.fromId != _chat.users[0].id
                            ? Bubble(
                                margin: BubbleEdges.only(top: 10),
                                alignment: Alignment.topLeft,
                                color: Color.fromRGBO(50, 50, 50, 1.0),
                                nip: BubbleNip.leftTop,
                                radius: Radius.circular(20.0),
                                child: Text(
                                  i.message != null ? i.message: "",
                                  style: TextStyle(color: Colors.white),
                                ),
                              )
                            : Bubble(
                                margin: BubbleEdges.only(top: 10),
                                alignment: Alignment.topRight,
                                nip: BubbleNip.rightBottom,
                                color: Color.fromRGBO(40, 98, 198, 1.0),
                                radius: Radius.circular(20.0),
                                child: ListTile(
                                  dense: true,

                                  title: Text(i.message != null ? i.message: "",
                                      textAlign: TextAlign.right,
                                      style: TextStyle(color: Colors.white)),
                                  trailing: i.loading ? Icon(Icons.access_time, color: Colors.white,): SizedBox(),
                                ),
                              ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.black45,
                border: Border.all(color:Colors.black, width: 1, style: BorderStyle.solid),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                      child: Container(
                        padding: const EdgeInsets.only(top: 10, bottom: 10, right: 10),
                        child: TextField(
                          controller: _controller,
                          maxLines: null,
                          style: TextStyle(color: Colors.white),
                          textAlign: TextAlign.center,
                          cursorColor: Colors.white,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Theme.of(context).accentColor,
                            disabledBorder: InputBorder.none,
                            alignLabelWithHint: true,
                            hintText: 'Message',
                            hintStyle: TextStyle(
                              color: Colors.grey,
                            ),
                            contentPadding:
                            const EdgeInsets.only(bottom: 10, top: 10, left: 10, right: 10),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(20),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black),
                              borderRadius: BorderRadius.circular(20),
                            ),
                          ),
                        ),
                      ),
                  ),
                  IconButton(
                    icon: Icon(Icons.send, color: Colors.white,),
                    onPressed: () {
                      Provider.of<ChatProvider>(context, listen: false).sendMessage(chatId, _controller.text, null).then((_){
                        _controller.text = "";
                      });

                    },
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
