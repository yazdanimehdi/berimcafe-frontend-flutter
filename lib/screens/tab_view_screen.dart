import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../screens/options_screen.dart';
import 'chat/message_screen.dart';
import '../screens/profile_screen.dart';
import '../locale/app_localization.dart';
import '../screens/chat/new_chat_screen.dart';
import '../providers/chat.dart';

class TabViewScreen extends StatefulWidget {
  static const routeName = '/tab-view';

  @override
  _TabViewScreenState createState() => _TabViewScreenState();
}

class _TabViewScreenState extends State<TabViewScreen> {
  var _isInit = true;
  var _isLoading = false;
  int _pageIndex = 0;

  void _changeTab(int index) {
    setState(() {
      _pageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_isInit) {
      if (ModalRoute.of(context).settings.arguments != null) {
        _changeTab(ModalRoute.of(context).settings.arguments);
      }
      _isInit = false;
    }

    final List<Map<String, Object>> _pages = [
      {
        'page': OptionsScreen(),
        'title': AppLocalizations.of(context).translate('home'),
      },
      {
        'page': MessageScreen(),
        'title': AppLocalizations.of(context).translate('messages')
      },
      {
        'page': ProfileScreen(),
        'title': AppLocalizations.of(context).translate('profile')
      }
    ];

    return Scaffold(
      appBar: AppBar(
        title: Text(
          _pages[_pageIndex]['title'],
          style: Theme.of(context).textTheme.title,
        ),
        elevation: 20,
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : _pages[_pageIndex]['page'],
      floatingActionButton: _pageIndex == 1
          ? FloatingActionButton(
              child: Icon(
                Icons.message,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.of(context)
                    .pushReplacementNamed(NewChatScreen.routeName);
              })
          : Container(),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _pageIndex,
        onTap: _changeTab,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text(
                AppLocalizations.of(context).translate('home'),
                style: TextStyle(fontWeight: FontWeight.bold),
              )),
          BottomNavigationBarItem(
              icon: Icon(Icons.email),
              title: Text(
                AppLocalizations.of(context).translate('messages'),
                style: TextStyle(fontWeight: FontWeight.bold),
              )),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle),
              title: Text(
                AppLocalizations.of(context).translate('profile'),
                style: TextStyle(fontWeight: FontWeight.bold),
              ))
        ],
      ),
    );
  }
}
