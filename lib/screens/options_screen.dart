import 'package:flutter/material.dart';
import '../widgets/options_widget.dart';
import '../locale/app_localization.dart';
import '../category_list.dart';

class OptionsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20),
      child: GridView(
        children: CATEGORIES.map((cat) {
          return OptionsWidget(text:AppLocalizations.of(context).translate(cat.name), id:cat.id, color: cat.color, icon: cat.icon,);
        }).toList(),
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 200,
          mainAxisSpacing: 5,
          crossAxisSpacing: 5,
          childAspectRatio: 3/2,
        ),
      ),
    );
  }
}
