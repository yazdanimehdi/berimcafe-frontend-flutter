class MutationsAll {
  String getAuthToken(String username, String password) {
    return """
      mutation{
        tokenAuth(username: \"$username\", password: \"$password\"){
          token
        }
      }
    """;
  }

  String addCustomer(String name, String email1, String email2, String address,
      String extraInfo, String notes, String phone1, String phone2) {
    return """mutation{
                createCustomer(name:\"$name\", phone1:\"$phone1\", phone2:\"$phone2\", email1:\"$email1\", email2:\"$email2\", address:\"$address\", extraInfo:\"$extraInfo\", notes:\"$notes\"){
  	                customer{
                        id
                      }
                    }
                } 
                """;
  }

  String deleteCustomer(String id) {
    return """
      mutation{
        deleteCustomer(customerId: $id){
            status
        }
      }
    """;
  }

  String editCustomer(String id, Map<String, String> editMap) {
    return """
      mutation{
          editCustomer(customerId:$id ,name:\"${editMap['name']}\", phone1:\"${editMap['phone1']}\", phone2:\"${editMap['phone2']}\", email1:\"${editMap['email1']}\", email2:\"${editMap['email2']}\", extraInfo:\"${editMap['extra_info']}\", notes:\"${editMap['notes']}\"){
            customer{
                id
            }
          }
       }
    """;
  }

  String addFoodCategory(String name) {
    return """ 
    mutation{
      createFoodCategory(name:"$name"){
		    foodCategory{
          id
        }
      }
    }
    """;
  }

  String editFoodCategory(String id, String name) {
    return """
      mutation{
        editFoodCategory(foodCategoryId:$id, name:"$name"){
          foodCategory{
            id
          }
        }
      }
    """;
  }

  String deleteFoodCategory(String id) {
    return """
      mutation{
        deleteFoodCategory(foodCategoryId:$id){
          status
          }
      }
    """;
  }

  String addIngredientCategory(String name) {
    return """
      mutation{
        createIngredientCategory(name:"$name"){
          ingredientCategory{
            id
          }
        }
      } 
    """;
  }

  String editIngredientsCategory(String id, String name) {
    return """
      mutation{
        editIngredientCategory(ingredientsCategoryId:$id,name:"$name"){
          ingredientsCategory{
            id
          }
        }
      }
    """;
  }

  String deleteIngredientsCategory(String id) {
    return """
    mutation{
      deleteIngredientCategory(ingredientsCategoryId:$id){
        status
      }
    }
    """;
  }

  String addIngredient(Map<String, String> data) {
    return """
      mutation{
        createIngredient(categoryId:${data['category_id']} , description:"${data['description']}", name: "${data['name']}", price:${data['price']}, scale:"${data['scale']}"){
          ingredient{
            id
          }
        }
      }
    """;
  }

  String deleteIngredient(String id) {
    return """
      mutation{
        deleteIngredient(ingredientId:$id){
          status
        }
      }
    """;
  }

  String addFoodIngredient(String foodId, String ingredientId, String amount) {
    return """
    mutation{
      createFoodIngredient(foodId:$foodId, ingredientId:$ingredientId, amount:$amount){
        foodIngredients{
          id
        }
        status
        message
      }
    }
    """;
  }

  String deleteFoodIngredient(String id) {
    return """
    mutation{
      deleteFoodIngredient(foodIngredientId:$id){
        status
      }
    }
    """;
  }

  String editFood(
      String foodId,
      String name,
      String price,
      String categoryId,
      String discount,
      String discountPercent,
      String description,
      String status,
      String relatedProductionPlace) {
    return """
      mutation{
        editFood(foodId:$foodId, categoryId:$categoryId, price:$price, discount:$discount, discountPercent:$discountPercent, description:"$description",
        foodStatus:$status, relatedProductionPlace:"$relatedProductionPlace"){
          food{
            id
          }
        }
      }
    """;
  }

  String addFood(Map<String, Object> _addMap) {
    return """
    mutation{
      createFood(categoryId:${_addMap['category_id']}, description:"${_addMap['description']}", discount:${_addMap['discount']}, discountPercent:${_addMap['discount_percent']}, foodStatus:${_addMap['status']}, name:"${_addMap['name']}", price:${_addMap['price']}, relatedProductionPlace:"${_addMap['related_production_place']}"){
        food{
          id
        }
      }
    }
    """;
  }

  String deleteFood(String id) {
    return """
      mutation{
        deleteFood(foodId:$id){
          status
        }
      }
    """;
  }

  String editIngredient(String id, String name, String price, String scale,
      String catId, String description) {
    return """
    mutation{
      editIngredient(ingredientId: $id, categoryId: $catId, name:"$name", price:$price, description:"$description", scale:"$scale"){
        status
      }
    }
    """;
  }

  String createInventoryEntry(String description, String amount,
      DateTime dateTime, String ingredientId, String cost) {
    return """
      mutation{
        createInventoryEntry(ingredientId:$ingredientId, description:"$description", dateTime:"${dateTime.toIso8601String()}", cost: $cost, amount:$amount){
          status
          message
          inventoryEntry{
            id
          }
        }
      }
    """;
  }

  String createInventoryOut(String description, String amount,
      DateTime dateTime, String ingredientId) {
    return """
      mutation{
        createInventoryOut(ingredientId:$ingredientId, description:"$description", dateTime:"${dateTime.toIso8601String()}", amount:$amount){
          status
          message
          inventoryOut{
            id
          }
        }
      }
    """;
  }

  String deleteInventoryEntry(String inventoryEntryId) {
    return """
      mutation{
        deleteInventoryEntry(inventoryEntryId:$inventoryEntryId){
          status
        }
      }
    """;
  }

  String deleteInventoryOut(String inventoryOutId) {
    return """
      mutation{
        deleteInventoryOut(inventoryOutId:$inventoryOutId){
          status
        }
      }
    """;
  }


  String createChatSession(List<int> userIds, String name) {
    return """
      mutation{
        createChatSession(usersId:$userIds ${name != null ? ", name: \"$name\"" : ''}){
          chatSession{
            id
          }
        }
      }
    """;
  }

  String sendReadMessage(String messageId) {
    return """
      mutation{
        readChatMessage(messageId:$messageId){
          status
        }
      }
    """;
  }

  String sendMessage(String chatId, String message, String replyId){
    if (replyId != null){
      return """
      mutation{
        createChatMessage(chatSessionId:$chatId, message:"$message", replyMessageId:$replyId){
          status
          messages{
            id
          }
          message
        }
      }
    """;
    }
    else{
      return """
      mutation{
        createChatMessage(chatSessionId:$chatId, message:"$message"){
          status
          messages{
            id
            dateTime
             fromUser{
              id
            }
          }
          message
        }
      }
    """;
    }

  }
}
