import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../locale/app_localization.dart';
import '../../providers/roles.dart';
import '../../models/personnel.dart';

class RolesScreen extends StatefulWidget {
  static const roteName = '/roles-screen';

  @override
  _RolesScreenState createState() => _RolesScreenState();
}

class _RolesScreenState extends State<RolesScreen> {
  var _isInit = true;
  var _isLoading = false;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<RolesProvider>(context).getAndFetchRoles().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final List<Roles> _rolesList =
        Provider.of<RolesProvider>(context).getRolesList;
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).translate('roles')),
        backgroundColor: Colors.black,
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: EdgeInsets.all(30),
              child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200,
                    childAspectRatio: 3 / 2,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10,
                  ),
                  itemCount: _rolesList.length,
                  itemBuilder: (BuildContext ctx, int index) {
                    MaterialColor _color = Colors
                        .primaries[Random().nextInt(Colors.primaries.length)];
                    return InkWell(
                      onTap: () {},
                      splashColor: _color,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          gradient: LinearGradient(
                            colors: [_color.withOpacity(0.5), _color],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ),
                        ),
                        child: GridTile(
                          header: Padding(
                            padding: const EdgeInsets.all(15),
                            child: FittedBox(
                              child: Text(
                                _rolesList[index].name,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                          child: Container(),
                        ),
                      ),
                    );
                  }),
            ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        onPressed: (){

        },
        child: Icon(Icons.add, color: Colors.white,),
      ),
    );
  }
}
