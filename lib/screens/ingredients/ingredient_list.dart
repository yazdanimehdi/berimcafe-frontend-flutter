import 'package:cafe_management_app/screens/ingredients/ingredient_edit_view_screen.dart';

import 'add_ingredient_screen.dart';

import '../../locale/app_localization.dart';

import '../../providers/locale.dart';

import '../../providers/ingredients.dart';
import '../../providers/foods_all.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/http_exceptions.dart';

class IngredientListScreen extends StatefulWidget {
  static const routeName = '/ingredients-list';

  @override
  _IngredientListScreenState createState() => _IngredientListScreenState();
}

class _IngredientListScreenState extends State<IngredientListScreen> {
  var _isLoading = false;

  void _showErrorDialog(String message) {
    showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title:
            Text(AppLocalizations.of(context).translate('login_error_title')),
        content: Text(message),
        actions: <Widget>[
          FlatButton(
            child: Text('Okay'),
            onPressed: () {
              Navigator.of(ctx).pop();
            },
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Future<void> _deleteIngredient(String id) async {
      try {
        await Provider.of<IngredientsProvider>(context, listen: false)
            .deleteIngredient(id);
      } on HttpException catch (error) {
        print(error.toString());
        var errorMessage = 'Editing failed';
        if (error.toString().contains('Please, enter valid credentials')) {
          errorMessage =
              AppLocalizations.of(context).translate('login_error_message');
        }
        _showErrorDialog(errorMessage);
      } catch (error) {
        print(error);
        var errorMessage =
            AppLocalizations.of(context).translate('connection_error');
        _showErrorDialog(errorMessage);
      }

      setState(() {
        _isLoading = false;
      });
    }

    final arguments = ModalRoute.of(context).settings.arguments as Map;
    final categoryIndex = arguments['index'];
    final MaterialColor _color = arguments['color'];
    final List _categoryList = Provider.of<IngredientsProvider>(context)
        .getIngredientsByCategory(categoryIndex)['all'];
    final IngredientsCategory _category =
        Provider.of<IngredientsProvider>(context).getCategoriesById(categoryIndex);
    return _isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : Scaffold(
            appBar: AppBar(
              title: Text(_category.name),
              backgroundColor: _color,
            ),
            body: ListView.separated(
                itemBuilder: (BuildContext ctx, int index) => Dismissible(
                      onDismissed: (_) =>
                          _deleteIngredient(_categoryList[index].id),
                      direction: Provider.of<LocaleProvider>(context).locale ==
                              Locale('en', 'US')
                          ? DismissDirection.endToStart
                          : DismissDirection.startToEnd,
                      key: UniqueKey(),
                      background: Container(
                        height: 80,
                        color: Theme.of(context).errorColor,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Icon(
                                Icons.delete,
                                color: Colors.white,
                                size: 28,
                              ),
                              SizedBox(
                                height: 2,
                              ),
                              Text(
                                AppLocalizations.of(context)
                                    .translate('delete'),
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          ),
                        ),
                        alignment: Alignment.centerRight,
                        padding: EdgeInsets.only(right: 20),
                      ),
                      child: ListTile(
                        title: Text(_categoryList[index].name),
                        subtitle: Text(_categoryList[index].scale),
                        trailing: Text(_categoryList[index].price.toString()),
                        onTap: (){
                          Navigator.of(context).pushNamed(IngredientEditViewScreen.routeName, arguments: {'ingredientId':_categoryList[index].id, 'color':_color, 'categoryName': _category.name, 'categoryId': _category.id});
                        },
                      ),
                    ),
                separatorBuilder: (context, index) => Divider(
                      height: 20,
                      color: Colors.black26,
                    ),
                itemCount: _categoryList.length),
            floatingActionButton: FloatingActionButton(
              backgroundColor: _color,
              onPressed: () {
                Navigator.of(context).pushNamed(AddIngredientScreen.routeName,
                    arguments: {'index': _category.id, 'color': _color});
              },
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
            ),
          );
  }
}
