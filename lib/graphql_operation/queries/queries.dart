class QueryAll {
  String getCustomersAll() {
    return """
    query{
      allCustomers{
        id
        name
        phone1
        phone2
        email1
        email1
        email2
        address
        extraInfo
        notes
      }
    }
    """;
  }

  String getMe() {
    return """
        query{
            me{
                id
            }
        }
     """;
  }
  String getFoodCategories(){
    return """
      query{
        allFoodCategory{
          id
          name
          foodSet{
            id
            name
            description
            relatedProductionPlace
            price
            discountPercent
            discount
            status
            foodingredientsSet{
              id
              amount
              ingredient{
                id
              }
           } 
          }
        }
      }
    """;
  }
  String getFoodWithoutCategory(){
    return """
      query{
        allFoodWithoutCategory{
          id
          name
          description
          price
          discountPercent
          discount
          status
          relatedProductionPlace
          foodingredientsSet{
              id
              amount
              ingredient{
                id
              }
           } 
        }
      }
    """;
  }
  String getIngredientsCategory(){
    return """
      query{
        allIngredientsCategory{
          id
          name
          ingredientsSet{
            id
            name
            description
            scale
            price
          }
        }
      }
    """;
  }
  String getIngredientsWithoutCategory(){
    return """
      query{
        allIngredientsWithoutCategory{
          id
          name
          scale
          description
          price
        }
      }
    """;
  }

  String getInventoryIngredient(){
    return """
      query{
        allInventoryIngredients{
          id
          ingredient{
           category{
            id
            }
            id
            scale
          }
          amount
        }
      }
    """;
  }

  String getInventoryEntries(){
    return """
      query{
        allInventoryEntry{
          id
          amount
          cost
          ingredient{
            id
          }
          description
          dateTime
        }
      }
    """;
  }

  String getInventoryOuts(){
    return """
      query{
        allInventoryOut{
          id
          amount
          ingredient{
            id
          }
          description
          dateTime
        }
      }
    """;
  }

  String getPersonnelAll(){
    return """
    query{
      allPersonnel{
        id
        username
        firstName
        lastName
        email
        phone
        rolepersonnelSet{
          role{
            id
            name
            roleRelated
            permissionsroleSet{
              permission{
                id
                permission
              }
            }
          }
        }
      }
    }
    """;
  }

  String getRolesAll(){
    return """
      query{
       allRoles{
        id
        name
        roleRelated
        permissionsroleSet{
          permission{
            id
            permission
          }
        }
      }
      }
    """;
  }

  String getAllChats(){
    return """
      query{
        allChats{
          users{
            id
            username
            firstName
            lastName
            rolepersonnelSet{
              role{
                name
              }
            }
          }
          id
          name
          chatmessagesSet{
            id
            fromUser{
              id
              username
            }
            replyMessage{
              id
            }
            message
            file
            read
            dateTime
          }
        }
      }
    """;
  }
}
