import 'package:flutter/material.dart';
import '../models/inventory.dart';
import '../services/graphQLConf.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../graphql_operation/queries/queries.dart';
import '../graphql_operation/mutations/mutations.dart';
import '../models/http_exceptions.dart';

class Inventory with ChangeNotifier {
  final String authToken;
  List<InventoryIngredient> _inventory;

  Inventory(this.authToken, this._inventory);

  List<InventoryIngredient> get getInventories {
    return [..._inventory];
  }

  List<InventoryIngredient> getInventoryByCategory(String id) {
    List<InventoryIngredient> _inventoryLoaded = [];
    _inventory.forEach((value) {
      if (value.categoryId == id) {
        _inventoryLoaded.add(value);
      }
    });
    return _inventoryLoaded;
  }

  InventoryIngredient getInventoryByIngredientId(String id){
    InventoryIngredient inv;
    _inventory.forEach((value){
      if(value.ingredientId == id){
        inv = value;
      }
    });
    return inv;
  }

  Future<void> fetchAndSetInventory() async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    QueryResult result = await _client.query(
      QueryOptions(documentNode: gql(QueryAll().getInventoryIngredient())),
    );
    final List<InventoryIngredient> _loadedInventory = [];
    try {
      if (!result.hasException) {
        if (result.data['allInventoryIngredients'] == null) {
          return [];
        }
        for (var i = 0;
        i < result.data['allInventoryIngredients'].length;
        i++) {
          InventoryIngredient _inv;
          _inv = InventoryIngredient(
            id: result.data['allInventoryIngredients'][i]['id'],
            amount: result.data['allInventoryIngredients'][i]['amount'],
            ingredientId: result.data['allInventoryIngredients'][i]
            ['ingredient']['id'],
            scale: result.data['allInventoryIngredients'][i]['ingredient']
            ['scale'],
            categoryId: result.data['allInventoryIngredients'][i]['ingredient']
            ['category']['id'],
          );
          _loadedInventory.add(_inv);
        }
        _inventory = _loadedInventory;
        notifyListeners();
      } else {
        throw HttpException(result.exception.toString());
      }
    } catch (error) {
      print(error);
      throw error;
    }
  }

  Future<void> addInventoryEntry(String description, String amount,
      DateTime dateTime, String ingredientId, String cost) async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    QueryResult result = await _client.mutate(
      MutationOptions(documentNode: gql(MutationsAll().createInventoryEntry(
          description, amount, dateTime, ingredientId, cost))),
    );
    try {
      if (!result.hasException) {
        if(result.data['createInventoryEntry']['status']){
          fetchAndSetInventory();
        }
        else{
          throw(HttpException(result.data['createInventoryEntry']['message']));
        }
      }
      else {
        throw(HttpException(result.exception.toString()));
      }
    }
    catch (error) {
      print(error);
      throw(error);
    }
  }

  Future<void> addInventoryOut(String description, String amount,
      DateTime dateTime, String ingredientId) async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    QueryResult result = await _client.mutate(
      MutationOptions(documentNode: gql(MutationsAll().createInventoryOut(
          description, amount, dateTime, ingredientId))),
    );
    try {
      if (!result.hasException) {
        if(result.data['createInventoryOut']['status']){
          fetchAndSetInventory();
        }
        else{
          throw(HttpException(result.data['createInventoryOut']['message']));
        }
      }
      else {
        throw(HttpException(result.exception.toString()));
      }
    }
    catch (error) {
      print(error);
      throw(error);
    }
  }
}
