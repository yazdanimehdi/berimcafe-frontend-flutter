import 'dart:math';
import 'package:provider/provider.dart';

import '../../locale/app_localization.dart';
import 'package:flutter/material.dart';
import '../../providers/customers.dart';
import './add_customers_screen.dart';
import './view_and_edit_customer_screen.dart';
import '../../providers/locale.dart';

class CustomersScreen extends StatefulWidget {
  static const routeName = '/customers';

  @override
  __AddCustomerScreenState createState() => __AddCustomerScreenState();
}

class __AddCustomerScreenState extends State<CustomersScreen> {
  var _isInit = true;
  var _isLoading = false;


  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<Customers>(context).fetchAndSetCustomers().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.redAccent,
          title: Text(
            AppLocalizations.of(context).translate('customers'),
            style: Theme.of(context).textTheme.title,
          ),
        ),
        body: _isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Consumer<Customers>(
                builder: (ctx, customers, _) => Container(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: ListView.separated(
                      separatorBuilder: (context, index) => Divider(
                            height: 20,
                            indent: 80,
                            color: Colors.black26,
                          ),
                      itemBuilder: (BuildContext ctx, int index) => Dismissible(
                        direction: Provider.of<LocaleProvider>(context).locale == Locale('en', 'US') ? DismissDirection.endToStart: DismissDirection.startToEnd,
                        onDismissed: (direction) => Provider.of<Customers>(context, listen: false).removeCustomers(customers.customers[index].id),
                        key: UniqueKey(),
                        background: Container(
                          height: 80,
                          color:Theme.of(context).errorColor,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Icon(Icons.delete, color: Colors.white, size: 28,),
                              SizedBox(height: 2,),
                              Text(AppLocalizations.of(context).translate('delete'), style: TextStyle(color: Colors.white),)
                            ],
                          ),
                          alignment: Alignment.centerRight,
                          padding: EdgeInsets.only(right: 20),
                        ),
                        child: ListTile(
                              onTap:() => Navigator.of(context).pushNamed(ViewEditCustomers.routName, arguments:customers.customers[index].id ),
                              leading: CircleAvatar(
                                radius: 30,
                                backgroundColor: Colors.primaries[
                                    Random().nextInt(Colors.primaries.length)],
                                child: Text("${index + 1}", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                              ),
                              title: Text(customers.customers[index].name),
                              trailing: Icon(Icons.arrow_forward_ios),
                            ),
                      ),
                      itemCount: customers.customers.length),
                ),
              ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add, color: Colors.white,),
          backgroundColor: Colors.redAccent,
          onPressed: (){
            Navigator.of(context).pushNamed(AddCustomerScreen.routeName);
          },
    )
              );
  }
}
