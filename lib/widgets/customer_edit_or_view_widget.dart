import 'package:flutter/services.dart';

import '../models/customer.dart';

import '../locale/app_localization.dart';
import 'package:flutter/material.dart';

class EditOrView extends StatelessWidget {
  final Customer customer;
  final IconData icon;
  final String title;
  final bool isEditing;
  final String initialValue;
  Map<String, String> editMap;
  final String keyName;

  EditOrView(
      {@required this.customer,
      @required this.icon,
      @required this.title,
      @required this.isEditing,
      @required this.initialValue,
      @required this.editMap,
      @required this.keyName});

  @override
  Widget build(BuildContext context) {
    return isEditing
        ? TextFormField(
            initialValue: initialValue,
            onSaved: (value) {
              editMap[keyName] = value;
            },
            decoration: InputDecoration(
              icon: Icon(icon),
              labelText: title,
              filled: true,
              fillColor: Colors.transparent,
            ),
          )
        : ListTile(
            leading: Icon(icon),
            title: Text(title),
            trailing: initialValue == null ? Text("") : Text(initialValue),
            onTap: () {},
          );
  }
}
