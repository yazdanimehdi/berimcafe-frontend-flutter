import 'package:flutter/foundation.dart';

class Customer{
  String id;
  String name;
  String phone1;
  String phone2;
  String email1;
  String email2;
  String address;
  String extraInfo;
  String notes;

  Customer({@required this.id, @required this.name, this.phone1, this.phone2, this.email1, this.email2, this.address, this.extraInfo, this.notes});
}