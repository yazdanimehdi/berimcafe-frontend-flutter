import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/locale.dart';


class SelectLanguageScreen extends StatefulWidget {
  static const routeName = '/lang-select';

  @override
  _SelectLanguageScreenState createState() => _SelectLanguageScreenState();
}

class _SelectLanguageScreenState extends State<SelectLanguageScreen> {
  var _isEnglishSelected = false;
  var _isFarsiSelected = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.black,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,

            children: <Widget>[
              Center(
                  child:
                  _isEnglishSelected ? Text('Cafe And Restaurant Management App', style: TextStyle(fontSize: 20, color: Colors.white),): Text('نرم‌افزار مدیریت کافه و رستوران', style: TextStyle(fontSize: 20, color: Colors.white))),
              SizedBox(height: 40,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.all(8),
                    child:ChoiceChip(
                      disabledColor: Colors.grey,
                      selectedColor: Colors.green,
                      label: Text('English'),
                      selected: _isEnglishSelected,
                      onSelected: (_){
                        setState(() {
                          _isEnglishSelected = true;
                          if(_isEnglishSelected){
                            _isFarsiSelected = false;
                          }
                        });
                      },
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(8),
                    child:ChoiceChip(
                      disabledColor: Colors.grey,
                      selectedColor: Colors.green,
                      label: Text('فارسی'),
                      selected: _isFarsiSelected,
                      onSelected: (_){
                        setState(() {
                          _isFarsiSelected = true;
                          if(_isFarsiSelected){
                            _isEnglishSelected = false;
                          }
                        });
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 40,),
              Consumer<LocaleProvider>(
                builder: (ctx, locale, _) => RaisedButton(
                  child: _isEnglishSelected ? Text('Change'): Text('تغییر'),
                  onPressed: (){
                    _isEnglishSelected ? locale.changeLocale(Locale('en', 'US')): locale.changeLocale(Locale('fa', 'IR'));
                  }
                  ,
                ),
              )
            ],
          ),
      ),
      );
  }
}
