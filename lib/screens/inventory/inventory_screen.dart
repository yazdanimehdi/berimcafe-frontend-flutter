import 'dart:math';

import '../../screens/inventory/inventory_category_screen.dart';

import '../../locale/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/ingredients.dart';
import '../../providers/foods_all.dart';
import '../../screens/inventory/inventory_io_screen.dart';


class InventoryScreen extends StatefulWidget {
  static const routeName = '/inventory-category';
  @override
  _InventoryScreenState createState() => _InventoryScreenState();
}

class _InventoryScreenState extends State<InventoryScreen> {
  var _isInit = true;
  var _isLoading = false;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<IngredientsProvider>(context).getAllIngredients().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).translate('inventory')),
        backgroundColor: Colors.teal,
        actions: <Widget>[
          IconButton(icon: Icon(Icons.archive, color: Colors.white,),
            onPressed: (){
              Navigator.of(context).pushNamed(InventoryIoScreen.routeName);
            },
          )
        ],
      ),
      body: _isLoading ? Center(child: CircularProgressIndicator(),) :
      Consumer<IngredientsProvider>(
        builder: (ctx, ingredient, _) =>
            Padding(
              padding: EdgeInsets.all(30),
              child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200,
                    childAspectRatio: 3 / 2,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10,
                  ),
                  itemCount: ingredient.getIngredients.length,
                  itemBuilder: (BuildContext ctx, int index) {
                    IngredientsCategory _category = ingredient.getIngredients[index]['category'];
                    MaterialColor _color = Colors
                        .primaries[Random().nextInt(Colors.primaries.length)];
                    return InkWell(
                      onTap: () {
                        Navigator.of(context).pushNamed(InventoryCategoryScreen.routeName,
                            arguments: {'index': _category.id, 'color': _color});
                      },
                      splashColor: _color,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          gradient: LinearGradient(colors: [_color
                              .withOpacity(0.5), _color],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,),

                        ),
                        child: GridTile(
                          header: Padding(
                            padding: const EdgeInsets.all(15),
                            child: FittedBox(
                              child: Text(
                                _category.name,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                          child: Container(),
                        ),
                      ),
                    );
                  }),
            ),
      ),
    );
  }
}
