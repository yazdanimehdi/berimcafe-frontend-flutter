import 'dart:math';

import '../../providers/foods_all.dart';
import 'ingredient_list.dart';
import '../../providers/ingredients.dart';
import 'package:provider/provider.dart';

import '../../locale/app_localization.dart';
import '../../models/http_exceptions.dart';
import 'package:flutter/material.dart';


class IngredientsCategoryScreen extends StatefulWidget {
  static const routeName = '/ingredients-category';

  @override
  _IngredientsCategoryScreenState createState() =>
      _IngredientsCategoryScreenState();
}

class _IngredientsCategoryScreenState extends State<IngredientsCategoryScreen> {
  var _isInit = true;
  var _isLoading = false;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<IngredientsProvider>(context).getAllIngredients().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    String _name;
    void _showErrorDialog(String message) {
      showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title:
          Text(AppLocalizations.of(context).translate('login_error_title')),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text(AppLocalizations.of(context).translate('ok')),
              onPressed: () {
                Navigator.of(ctx).pop();
              },
            )
          ],
        ),
      );
    }
    void _submit(String id) async{
      if (id == 'Create') {
        if (_formKey.currentState.validate()) {
          _formKey.currentState.save();
          try {
            await Provider.of<IngredientsProvider>(context, listen: false)
                .addIngredientCategory(_name);
            Navigator.of(context).pop();
          }
          on HttpException catch (_) {
            var errorMessage =
            AppLocalizations.of(context)
                .translate('connection_error');
            _showErrorDialog(errorMessage);
          }
          setState(() {
            _isLoading = false;

          });
        }
      }
      else{
        if (_formKey.currentState.validate()) {
          _formKey.currentState.save();
          try {
            await Provider.of<IngredientsProvider>(context, listen: false)
                .editIngredientCategory(id, _name);
            Navigator.of(context).pop();
          }
          on HttpException catch (_) {
            var errorMessage =
            AppLocalizations.of(context)
                .translate('connection_error');
            _showErrorDialog(errorMessage);
          }
          setState(() {
            _isLoading = false;
          });
        }

      }
    }
    void _editCreateCategory(IngredientsCategory _category) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Form(
                key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(_category == null ? AppLocalizations.of(context).translate('add_ingredientsـcategory'): AppLocalizations.of(context).translate('edit_ingredientsـcategory')),
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: TextFormField(
                        onSaved:(value){
                          _name = value;
                        } ,
                        initialValue: _category == null ? '': _category.name,
                        decoration: InputDecoration(
                          labelText: AppLocalizations.of(context).translate('name'),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                          child: Text(AppLocalizations.of(context).translate('submit')),
                          onPressed: () => _category == null ? _submit('Create'): _submit(_category.id)
                      ),
                    )
                  ],
                ),
              ),
            );
          });
    }

    void _deleteCategory(String id) async{
      try {
        await Provider.of<IngredientsProvider>(context, listen: false).deleteIngredientCategory(id);
      }
      on HttpException catch (_) {
        var errorMessage =
        AppLocalizations.of(context)
            .translate('connection_error');
        _showErrorDialog(errorMessage);
      }
      setState(() {
        _isLoading = false;
      });
    }
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.purpleAccent,
          title: Text(
            AppLocalizations.of(context).translate('ingredientsـcategory'),
            style: Theme
                .of(context)
                .textTheme
                .title,
          ),
        ),
        body: _isLoading
            ? Center(
          child: CircularProgressIndicator(),
        ) : Consumer<IngredientsProvider>(
          builder: (ctx, ingredient, _) =>
              Padding(
                padding: EdgeInsets.all(30),
                child: GridView.builder(
                    gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: 200,
                      childAspectRatio: 3 / 2,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 10,
                    ),
                    itemCount: ingredient.getIngredients.length,
                    itemBuilder: (BuildContext ctx, int index) {
                      IngredientsCategory _category = ingredient.getIngredients[index]['category'];
                      MaterialColor _color = Colors
                          .primaries[Random().nextInt(Colors.primaries.length)];
                      return InkWell(
                        onTap: () {
                          Navigator.of(context).pushNamed(IngredientListScreen.routeName,
                              arguments: {'index': _category.id, 'color': _color});
                        },
                        splashColor: _color,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            gradient: LinearGradient(colors: [_color
                                .withOpacity(0.5), _color],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,),

                          ),
                          child: GridTile(
                            header: Padding(
                              padding: const EdgeInsets.all(15),
                              child: FittedBox(
                                child: Text(
                                  _category.name,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                ),
                              ),
                            ),
                            footer: _category.name != 'Undefined' ? Padding(
                              padding: const EdgeInsets.all(5),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.black26,
                                    borderRadius: BorderRadius.circular(10)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment
                                      .spaceBetween,
                                  children: <Widget>[
                                    IconButton(
                                      icon: Icon(
                                        Icons.edit, color: Colors.white,),
                                      color: Colors.white,
                                      onPressed: () => _editCreateCategory(_category),
                                    ),
                                    IconButton(
                                      icon: Icon(
                                        Icons.delete, color: Colors.white,),
                                      onPressed: () => _deleteCategory(_category.id),
                                    ),
                                  ],
                                ),
                              ),
                            ): Container(),
                            child: Container(),
                          ),
                        ),
                      );
                    }),
              ),
        ),
        floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.purpleAccent,
            child: Icon(Icons.add, color: Colors.white,),
            onPressed: () => _editCreateCategory(null)
        ));
  }
}
