import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import '../../providers/inventory_io.dart';
import '../../providers/ingredients.dart';
import '../../providers/locale.dart';
import '../../locale/app_localization.dart';
import '../../models/inventory.dart';
import 'inventory_io_filter_screen.dart';
class InventoryIoScreen extends StatefulWidget {
  static const routeName = '/inventory-io';

  @override
  _InventoryIoScreenState createState() => _InventoryIoScreenState();
}

class _InventoryIoScreenState extends State<InventoryIoScreen> {
  var _isInit = true;
  var _isLoading = false;
  var _searchMode = false;
  var _searchController = TextEditingController();

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<InventoryIO>(context).getAndFetchInventoryIO().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  void dispose(){
    _searchController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    List<Object> _inventoryIO =
        Provider.of<InventoryIO>(context).getInventoryIO;

    return Scaffold(
      appBar: _searchMode ? AppBar(
        backgroundColor: Colors.teal,
        title: TextField(
          style: TextStyle(color: Colors.white),
          controller: _searchController,
          cursorColor: Colors.white,
          decoration: InputDecoration(
            labelText: AppLocalizations.of(context).translate('search'),
            labelStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search, color: Colors.white,),
            onPressed: (){

            },
          ),
          IconButton(
            icon: Icon(Icons.cancel, color: Colors.white,),
            onPressed: (){
              setState(() {
                _searchMode = false;
              });
            },
          )
        ],
      ):AppBar(
        title: Text(AppLocalizations.of(context).translate('inventory_io')),
        backgroundColor: Colors.teal,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.filter_list,
              color: Colors.white,
            ),
            onPressed: (){
              Navigator.of(context).pushNamed(InventoryIOFilterScreen.routeName);
            },
          ),
          IconButton(
            icon: Icon(Icons.search, color: Colors.white,),
            onPressed: (){
              setState(() {
                _searchMode = true;
              });
            },
          )
        ],
      ),
      body: _isLoading
          ? Center(child: CircularProgressIndicator())
          : Container(
              padding: EdgeInsets.all(20),
              child: ListView.separated(
                  itemBuilder: (BuildContext ctx, int index) {
                    if (_inventoryIO[index] is InventoryEntry) {
                      InventoryEntry _inv = _inventoryIO[index];
                      return Dismissible(
                        direction:
                            Provider.of<LocaleProvider>(context).locale ==
                                    Locale('en', 'US')
                                ? DismissDirection.endToStart
                                : DismissDirection.startToEnd,
                        onDismissed: (direction) {
                          _inventoryIO.removeAt(index);
                          Provider.of<InventoryIO>(context, listen: false)
                              .deleteInventoryEntry(_inv.id);
                        },
                        key: UniqueKey(),
                        background: Container(
                          height: 80,
                          color: Theme.of(context).errorColor,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Icon(
                                  Icons.delete,
                                  color: Colors.white,
                                  size: 28,
                                ),
                                SizedBox(
                                  height: 2,
                                ),
                                Text(
                                  AppLocalizations.of(context)
                                      .translate('delete'),
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                          ),
                          alignment: Alignment.centerRight,
                          padding: EdgeInsets.only(right: 20),
                        ),
                        child: Container(
                          color: Colors.green.withOpacity(0.8),
                          child: ListTile(
                            isThreeLine: true,
                            title: Text(
                              "${_inv.amount.toString()} (${Provider.of<IngredientsProvider>(context).getIngredientsById(_inv.ingredientId).scale}) ${Provider.of<IngredientsProvider>(context).getIngredientsById(_inv.ingredientId).name}",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            subtitle: Text(_inv.description == null
                                ? ''
                                : _inv.description),
                            trailing: Column(
                              children: <Widget>[
                                Text(_inv.cost.toString()),
                                Text(DateFormat.yMd()
                                    .add_Hms()
                                    .format(_inv.dateTime))
                              ],
                            ),
                          ),
                        ),
                      );
                    } else {
                      InventoryOut _inv = _inventoryIO[index];
                      return Dismissible(
                        direction:
                            Provider.of<LocaleProvider>(context).locale ==
                                    Locale('en', 'US')
                                ? DismissDirection.endToStart
                                : DismissDirection.startToEnd,
                        onDismissed: (direction) {
                          _inventoryIO.removeAt(index);
                          Provider.of<InventoryIO>(context, listen: false)
                              .deleteInventoryOut(_inv.id);
                        },
                        key: UniqueKey(),
                        background: Container(
                          height: 80,
                          color: Theme.of(context).errorColor,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Icon(
                                  Icons.delete,
                                  color: Colors.white,
                                  size: 28,
                                ),
                                SizedBox(
                                  height: 2,
                                ),
                                Text(
                                  AppLocalizations.of(context)
                                      .translate('delete'),
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                          ),
                          alignment: Alignment.centerRight,
                          padding: EdgeInsets.only(right: 20),
                        ),
                        child: Container(
                            color: Colors.red.withOpacity(0.8),
                            child: ListTile(
                              isThreeLine: true,
                              title: Text(
                                "${_inv.amount.toString()} (${Provider.of<IngredientsProvider>(context).getIngredientsById(_inv.ingredientId).scale} ${Provider.of<IngredientsProvider>(context).getIngredientsById(_inv.ingredientId).name})",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(_inv.description == null
                                  ? ''
                                  : _inv.description),
                              trailing: Text(DateFormat.yMd()
                                  .add_Hms()
                                  .format(_inv.dateTime)),
                            )),
                      );
                    }
                  },
                  separatorBuilder: (context, index) => Divider(
                        height: 5,
                        color: Colors.black26,
                      ),
                  itemCount: _inventoryIO.length),
            ),
    );
  }
}
