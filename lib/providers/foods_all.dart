import 'package:flutter/foundation.dart';

class Foods with ChangeNotifier {
  String id;
  String name;
  String description;
  double price;
  String category;
  int discountPercent;
  int discount;
  bool status;
  String relatedProductionPlace;

  Foods(
      {this.id,
      this.name,
      this.description,
      this.price,
      this.category,
      this.discount,
      this.discountPercent,
      this.relatedProductionPlace,
      this.status});
}

class FoodCategory with ChangeNotifier {
  String id;
  String name;

  FoodCategory(this.id, this.name);
}

class Ingredients with ChangeNotifier {
  String id;
  String name;
  String scale;
  String description;
  int price;
  Ingredients({this.id, this.name, this.scale, this.description, this.price});
}

class FoodIngredients with ChangeNotifier {
  String id;
  String ingredientId;
  String foodId;
  double amount;

  FoodIngredients({this.id, this.ingredientId, this.foodId, this.amount});
}

class IngredientsCategory with ChangeNotifier {
  String id;
  String name;

  IngredientsCategory(this.id, this.name);
}