import '../../screens/foods/food_list.dart';
import 'package:cafe_management_app/providers/locale.dart';
import '../../models/http_exceptions.dart';

import '../../providers/ingredients.dart';
import 'package:provider/provider.dart';
import '../../providers/foods.dart';
import '../../locale/app_localization.dart';
import 'package:flutter/material.dart';

class FoodAddScreen extends StatefulWidget {
  static const routeName = '/food-add';
  @override
  _FoodAddScreenState createState() => _FoodAddScreenState();
}

class _FoodAddScreenState extends State<FoodAddScreen> {
  String dropdownValueProductionPlace;
  String productionPlaceValue;
  String dropdownValueCategory;
  String dropdownValueStatus;
  final GlobalKey<FormState> _formKey = GlobalKey();
  final GlobalKey<FormState> _form2key = GlobalKey();
  String statusFinal;
  List<dynamic> _ingredientsList = [];
  var _isInit = true;
  var _isLoading = false;
  Map<String, Object> _formData = {
    "name": "",
    "description": "",
    "discount": "0",
    "discount_percent": "0",
    "related_production_place": "KI",
    "status": "true",
    "price": "",
    "category_id":  "",
    "ingredientList": [],
  };


  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context).settings.arguments as Map;
    final MaterialColor _color = arguments['color'];
    final _categoryId = arguments['category'];
    final _productionPlace = [AppLocalizations.of(context).translate('kitchen'), AppLocalizations.of(context).translate('bar')];
    final _status = [AppLocalizations.of(context).translate('enable'), AppLocalizations.of(context).translate('disable')];
    if (_isInit){
      setState(() {
        dropdownValueProductionPlace = _productionPlace[0];
        dropdownValueStatus = _status[0];
        statusFinal = true.toString();
        productionPlaceValue = 'KI';
        _isInit = false;
      });
    }

    void _submitAddIngredient(Map<String, String> myMap){
      setState(() {
        _ingredientsList.add(myMap);
      });
      Navigator.of(context).pop();
    }

    void _showErrorDialog(String message) {
      showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title:
          Text(AppLocalizations.of(context).translate('login_error_title')),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(ctx).pop();
              },
            )
          ],
        ),
      );
    }

    Future<void> _submit() async {
      if (!_formKey.currentState.validate()) {
        // Invalid!
        return;
      }
      _formKey.currentState.save();
      _formData['category_id'] = _categoryId;
      _formData['related_production_place'] = productionPlaceValue;
      _formData['status'] = statusFinal;
      _formData['ingredientList'] = _ingredientsList;
      setState(() {
        _isLoading = true;
      });
      try {
        await Provider.of<FoodsAll>(context, listen: false)
            .addFood(_formData);
      } on HttpException catch (error) {
        print(error.toString());
        var errorMessage = 'Editing failed';
        if (error.toString().contains('Please, enter valid credentials')) {
          errorMessage =
              AppLocalizations.of(context).translate('login_error_message');
        }
        _showErrorDialog(errorMessage);
      } catch (error) {
        print(error);
        var errorMessage =
        AppLocalizations.of(context).translate('connection_error');
        _showErrorDialog(errorMessage);
      }

      setState(() {
        _isLoading = false;
        Navigator.of(context).pop();
      });
    }


    void _addIngredient(String ingredientCat, String ingredient) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            String dropdownValueCategory = ingredientCat;
            String dropdownValueIngredient = ingredient;
            Map<String, List<Object>> ingredientName;
            String amount;
            List<String> ingredients;
            String ingredientId;
            final List<String> _ingredientCat =
            Provider.of<IngredientsProvider>(context, listen: false)
                .getCategories['names'];
            return AlertDialog(
              content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) =>
                    SingleChildScrollView(
                      child: Form(
                        key: _form2key,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(AppLocalizations.of(context)
                                .translate('add_ingredient')),
                            Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Column(
                                  children: <Widget>[
                                    Text(AppLocalizations.of(context)
                                        .translate('ingredientsـcategory')),
                                    DropdownButton(
                                      value: dropdownValueCategory,
                                      icon: Icon(Icons.arrow_downward),
                                      iconSize: 24,
                                      elevation: 16,
                                      onChanged: (String newValue) {
                                        int _index =
                                        _ingredientCat.indexOf(newValue);
                                        setState(() {
                                          ingredientName = Provider.of<
                                              IngredientsProvider>(context,
                                              listen: false)
                                              .getIngredientsByCategory(
                                              Provider.of<IngredientsProvider>(
                                                  context,
                                                  listen: false)
                                                  .getCategories['ids']
                                              [_index]);
                                          ingredients = ingredientName['names'];
                                          dropdownValueCategory = newValue;
                                        });
                                      },
                                      items: _ingredientCat
                                          .map<DropdownMenuItem<String>>(
                                              (String value) {
                                            return DropdownMenuItem<String>(
                                              value: value,
                                              child: Text(value),
                                            );
                                          }).toList(),
                                    ),
                                    Text(AppLocalizations.of(context)
                                        .translate('ingredient')),
                                    DropdownButton(
                                      value: dropdownValueIngredient,
                                      icon: Icon(Icons.arrow_downward),
                                      iconSize: 24,
                                      elevation: 16,
                                      onChanged: ingredientName == null
                                          ? (_) {}
                                          : (String newValue) {
                                        int _index = ingredientName['names']
                                            .indexOf(newValue);

                                        setState(() {
                                          dropdownValueIngredient = newValue;
                                          ingredientId =
                                          ingredientName['ids'][_index];
                                        });
                                      },
                                      items: ingredients == null
                                          ? null
                                          : ingredients
                                          .map<DropdownMenuItem<String>>(
                                              (String value) {
                                            return DropdownMenuItem<String>(
                                              value: value,
                                              child: Text(value),
                                            );
                                          }).toList(),
                                    ),
                                    TextFormField(
                                      keyboardType: TextInputType.number,
                                      validator: (value){
                                        int _intValue;
                                        try{
                                          _intValue = int.parse(value);
                                          return null;
                                        }
                                        catch(error){
                                          return AppLocalizations.of(context)
                                              .translate('input_english');
                                        }
                                      },
                                      onSaved: (value) {
                                        amount = value;
                                      },
                                      decoration: InputDecoration(
                                        labelText: AppLocalizations.of(context)
                                            .translate('amount'),
                                      ),
                                    )
                                  ],
                                )),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: RaisedButton(
                                  child: Text(AppLocalizations.of(context)
                                      .translate('submit')),
                                  onPressed: () {
                                    if (!_form2key.currentState.validate()) {
                                      // Invalid!
                                      return;
                                    }
                                    _form2key.currentState.save();
                                    _submitAddIngredient({'ingredientId':ingredientId, 'amount':amount, 'ingredientName':dropdownValueIngredient});
                                  }),
                            )
                          ],
                        ),
                      ),
                    ),
              ),
            );
          });
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).translate('add_food')),
        backgroundColor: _color,
      ),
      body: _isLoading ? Center(child: CircularProgressIndicator(),):
      SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      TextFormField(
                        onSaved: (value){
                          _formData['name'] = value;
                        },
                        decoration: InputDecoration(
                          labelText: AppLocalizations.of(context).translate('name')
                        ),
                      ),
                      SizedBox(height: 10,),
                      TextFormField(
                        validator: (value){
                          int _intValue;
                          try{
                            _intValue = int.parse(value);
                            return null;
                          }
                          catch(error){
                            return AppLocalizations.of(context)
                                .translate('input_english');
                          }
                        },
                        onSaved: (value){
                          _formData['price'] = value;
                        },
                        decoration: InputDecoration(
                            labelText: AppLocalizations.of(context).translate('price')
                        ),
                        keyboardType: TextInputType.number,
                      ),
                      SizedBox(height: 10,),
                      Text(AppLocalizations.of(context).translate('related_production_place')),
                      DropdownButton(
                        value: dropdownValueProductionPlace,
                        icon: Icon(Icons.arrow_downward),
                        iconSize: 24,
                        elevation: 16,
                        onChanged: (String newValue) {
                          if (_productionPlace.indexOf(newValue) ==
                              0) {
                            productionPlaceValue = 'KI';
                          } else {
                            productionPlaceValue = "BA";
                          }
                          setState(() {
                            dropdownValueProductionPlace = newValue;
                          });
                        },
                        items: _productionPlace
                            .map<DropdownMenuItem<String>>(
                                (String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                      ),
                      TextFormField(
                        validator: (value){
                          int _intValue;
                          try{
                            _intValue = int.parse(value);
                            return null;
                          }
                          catch(error){
                            return AppLocalizations.of(context)
                                .translate('input_english');
                          }
                        },
                        onSaved: (value){
                          _formData['discount'] = value;
                        },
                        initialValue: '0',
                        decoration: InputDecoration(
                            labelText: AppLocalizations.of(context).translate('discount')
                        ),
                        keyboardType: TextInputType.number,
                      ),
                      TextFormField(
                        validator: (value){
                          int _intValue;
                          try{
                            _intValue = int.parse(value);
                            return null;
                          }
                          catch(error){
                            return AppLocalizations.of(context)
                                .translate('input_english');
                          }
                        },
                        onSaved: (value){
                          _formData['discount_percent'] = value;
                        },
                        initialValue: '0',
                        decoration: InputDecoration(
                            labelText: AppLocalizations.of(context).translate('discount_percent')
                        ),
                        keyboardType: TextInputType.number,
                      ),
                      SizedBox(height: 10,),
                      Text(AppLocalizations.of(context).translate('status')),
                      DropdownButton(
                        value: dropdownValueStatus,
                        icon: Icon(Icons.arrow_downward),
                        iconSize: 24,
                        elevation: 16,
                        onChanged: (String newValue) {
                          if (_status.indexOf(newValue) ==
                              0) {
                            statusFinal = true.toString();
                          } else {
                            statusFinal = false.toString();
                          }
                          setState(() {
                            dropdownValueStatus = newValue;
                          });
                        },
                        items: _status
                            .map<DropdownMenuItem<String>>(
                                (String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                      ),
                      TextFormField(
                        onSaved: (value){
                          _formData['description'] = value;
                        },
                        decoration: InputDecoration(
                            labelText: AppLocalizations.of(context).translate('description')
                        ),
                        maxLines: 3,
                        keyboardType: TextInputType.multiline,
                      ),

                    ],
                  ),
                Divider(),

                      Container(
                        height: 200,
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment:
                              MainAxisAlignment.center,
                              children: <Widget>[
                                Text(AppLocalizations.of(context)
                                    .translate('ingredient')),
                                IconButton(
                                  icon: Icon(
                                    Icons.add,
                                    color: Colors.green,
                                  ),
                                  onPressed: () =>
                                      _addIngredient(null, null),
                                )
                              ],
                            ),
                            Expanded(
                              child: Consumer<IngredientsProvider>(
                                builder: (ctx, ingredient, _) =>
                                    ListView.separated(
                                        itemBuilder: (BuildContext ctx,
                                            int index) {
                                          return Dismissible(
                                            direction:
                                            Provider.of<LocaleProvider>(
                                                context)
                                                .locale ==
                                                Locale(
                                                    'en', 'US')
                                                ? DismissDirection
                                                .endToStart
                                                : DismissDirection
                                                .startToEnd,
                                            key: ValueKey(_ingredientsList[index]),
                                            onDismissed: (direction) {
                                              _ingredientsList.removeAt(index);
                                            },
                                            background: Container(
                                              height: 100,
                                              color: Theme.of(context)
                                                  .errorColor,
                                              child: Padding(
                                                padding:
                                                const EdgeInsets
                                                    .only(top: 10),
                                                child: Column(
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment
                                                      .start,
                                                  children: <Widget>[
                                                    Icon(
                                                      Icons.delete,
                                                      color:
                                                      Colors.white,
                                                      size: 22,
                                                    ),
                                                    SizedBox(
                                                      height: 2,
                                                    ),
                                                    Text(
                                                      AppLocalizations.of(
                                                          context)
                                                          .translate(
                                                          'delete'),
                                                      style: TextStyle(
                                                          color: Colors
                                                              .white),
                                                    )
                                                  ],
                                                ),
                                              ),
                                              alignment:
                                              Alignment.centerRight,
                                              padding: EdgeInsets.only(
                                                  right: 20),
                                            ),
                                            child: ListTile(
                                              title: Text(
                                                  _ingredientsList[index]['ingredientName']),
                                              trailing: Column(
                                                children: <Widget>[
                                                  Text(_ingredientsList[index]['amount'] == null ? '': _ingredientsList[index]['amount']),
                                                  Text(ingredient.getIngredientsById(_ingredientsList[index]['ingredientId'])
                                                      .scale)
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                        separatorBuilder:
                                            (context, index) => Divider(
                                          height: 20,
                                          color: Colors.black26,
                                        ),
                                        itemCount: _ingredientsList.length),
                              ),
                            ),
                          ],
                        ),
                      ),
                RaisedButton(
                  child: Text(AppLocalizations.of(context).translate('submit')),
                  onPressed: () => _submit(),
                )

              ],
            ),
          ),
        ),
      )
    );
  }
}
