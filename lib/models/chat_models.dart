class Chat{
  String id;
  String name;
  DateTime dateTime;
  List<ChatUser> users;
  List<ChatMessages> messages;

  Chat({this.id, this.dateTime, this.users, this.messages, this.name});
}

class ChatMessages{
  String id;
  DateTime dateTime;
  String repliedToId;
  String message;
  String fromId;
  bool read;
  bool loading;

  ChatMessages({this.id, this.dateTime, this.repliedToId, this.message, this.fromId, this.read, this.loading});
}

class ChatUser{
  String id;
  String userName;
  String firstName;
  String lastName;
  List<String> roleNames;

  ChatUser({this.id, this.userName, this.firstName, this.lastName, this.roleNames});
}