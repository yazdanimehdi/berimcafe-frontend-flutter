import '../../locale/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/customers.dart';
import '../../widgets/customer_edit_or_view_widget.dart';
import '../../models/customer.dart';
import '../../models/http_exceptions.dart';

class ViewEditCustomers extends StatefulWidget {
  static const routName = '/customer-edit';

  @override
  _ViewEditCustomersState createState() => _ViewEditCustomersState();
}

class _ViewEditCustomersState extends State<ViewEditCustomers> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  var _isEditing = false;
  var _isLoading = false;

  void _toggleEditing() {
    setState(() {
      _isEditing = !_isEditing;
    });
  }


  Widget buildBody(
      BuildContext context, String _customerId, Customer _customer, bool _isEditing) {
    Map<String, String> editMap = {
    'name': _customer.name,
      'phone1': _customer.phone1,
      'phone2': _customer.phone2,
      'email1': _customer.email1,
      'email2': _customer.email2,
      'extra_info': _customer.extraInfo,
      'notes': _customer.notes,
    };

    void _showErrorDialog(String message) {
      showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text(AppLocalizations.of(context).translate('login_error_title')),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(ctx).pop();
              },
            )
          ],
        ),
      );
    }
    Future<void> _submit() async {
      if (!_formKey.currentState.validate()) {
        // Invalid!
        return;
      }
      _formKey.currentState.save();
      setState(() {
        _isLoading = true;
      });
      try {
        await Provider.of<Customers>(context, listen: false).editCustomers(
            _customer.id,
            editMap
        );

      } on HttpException catch (error) {
        print(error.toString());
        var errorMessage = 'Editing failed';
        if (error.toString().contains('Please, enter valid credentials')) {
          errorMessage = AppLocalizations.of(context).translate('login_error_message');
        }
        _showErrorDialog(errorMessage);
      } catch (error) {
        print(error);
        var errorMessage = AppLocalizations.of(context).translate('connection_error');
        _showErrorDialog(errorMessage);
      }

      setState(() {
        _isLoading = false;
      });
      Navigator.of(context, rootNavigator: true)..pop();
    }
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                backgroundColor: Colors.redAccent,
                radius: 50,
                child: Text(
                  _customer.name[0],
                  style: TextStyle(color: Colors.white, fontSize: 35),
                ),
              ),
              Container(
                width: 200,
                child: _isEditing ? TextFormField(
                  initialValue: _customer.name,
                  onSaved: (value) {
                    editMap['name'] = value;
                  },
                  decoration: InputDecoration(
                    labelText: AppLocalizations.of(context).translate('name'),
                    filled: true,
                    fillColor: Colors.transparent,
                  ),
                ):Text(
                  _customer.name,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 28),
                ),
              )
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Expanded(
            child: ListView(
              children: <Widget>[
                EditOrView(
                  icon: Icons.phone,
                  title: AppLocalizations.of(context).translate('phone1'),
                  editMap: editMap,
                  keyName: 'phone1',
                  isEditing: _isEditing,
                  initialValue: _customer.phone1,
                  customer: _customer,
                ),
                EditOrView(
                  icon: Icons.phone,
                  editMap: editMap,
                  keyName: 'phone2',
                  title: AppLocalizations.of(context).translate('phone2'),
                  isEditing: _isEditing,
                  initialValue: _customer.phone2,
                  customer: _customer,
                ),
                EditOrView(
                  icon: Icons.email,
                  editMap: editMap,
                  keyName: 'email1',
                  title: AppLocalizations.of(context).translate('email1'),
                  isEditing: _isEditing,
                  initialValue: _customer.email1,
                  customer: _customer,
                ),
                EditOrView(
                  icon: Icons.email,
                  editMap: editMap,
                  keyName: 'email2',
                  title: AppLocalizations.of(context).translate('email2'),
                  isEditing: _isEditing,
                  initialValue: _customer.email2,
                  customer: _customer,
                ),
                EditOrView(
                  icon: Icons.home,
                  editMap: editMap,
                  keyName: 'address',
                  title: AppLocalizations.of(context).translate('address'),
                  isEditing: _isEditing,
                  initialValue: _customer.address,
                  customer: _customer,
                ),
                EditOrView(
                  icon: Icons.info,
                  editMap: editMap,
                  keyName: 'extra_info',
                  title: AppLocalizations.of(context).translate('extra_info'),
                  isEditing: _isEditing,
                  initialValue: _customer.extraInfo,
                  customer: _customer,
                ),
                EditOrView(
                  icon: Icons.note,
                  editMap: editMap,
                  keyName: 'notes',
                  title: AppLocalizations.of(context).translate('notes'),
                  isEditing: _isEditing,
                  initialValue: _customer.notes,
                  customer: _customer,
                ),
                _isEditing ?RaisedButton(
                  child: Text(AppLocalizations.of(context).translate('submit')),
                  onPressed: _submit,
                ): SizedBox(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final _customerId = ModalRoute.of(context).settings.arguments as String;
    var _customer = Provider.of<Customers>(context).customerById(_customerId);

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.redAccent,
          title: Text(
            '',
            style: Theme.of(context).textTheme.title,
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                AppLocalizations.of(context).translate('edit'),
                style: TextStyle(color: Colors.white),
              ),
              onPressed: _toggleEditing,
            )
          ],
        ),
        body: _isEditing
            ? Form(
                key: _formKey,
                child: buildBody(context, _customerId, _customer, _isEditing),
              )
            : buildBody(context, _customerId, _customer, _isEditing)
    );
  }
}
