import 'package:cafe_management_app/graphql_operation/mutations/mutations.dart';

import '../graphql_operation/queries/queries.dart';
import './foods_all.dart';
import '../services/graphQLConf.dart';
import '../models/http_exceptions.dart';
import 'package:flutter/foundation.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class FoodsAll with ChangeNotifier {
  final String authToken;
  List<Map<String, Object>> _categoryList;

  FoodsAll(this.authToken, this._categoryList);

  List<Map<String, Object>> get getCategories {
    return [..._categoryList];
  }
  Map<String, List<String>> get getCategoriesList{
    List<String> _myList = [];
    List<String> _myListId = [];
    _categoryList.forEach((element){
      FoodCategory _cat = element['category'];
      _myList.add(_cat.name);
      _myListId.add(_cat.id);
    });
    return {'names': _myList, 'ids':_myListId};
  }

  Future<void> fetchAndSetFoodCategory() async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    QueryResult result = await _client.query(
      QueryOptions(documentNode: gql(QueryAll().getFoodCategories())),
    );
    final List<Map<String, Object>> _loadedFoodCategory = [];
    try {
      if (!result.hasException) {
        for (var i = 0; i < result.data['allFoodCategory'].length; i++) {
          FoodCategory _category = FoodCategory(
              result.data['allFoodCategory'][i]['id'],
              result.data['allFoodCategory'][i]['name']);

          final List<Map<String, Object>> _foodsList = [];
          for (var j = 0;
              j < result.data['allFoodCategory'][i]['foodSet'].length;
              j++) {
            Foods food = Foods(
                id: result.data['allFoodCategory'][i]['foodSet'][j]['id'],
                name: result.data['allFoodCategory'][i]['foodSet'][j]['name'],
                price: result.data['allFoodCategory'][i]['foodSet'][j]['price'],
                relatedProductionPlace: result.data['allFoodCategory'][i]
                    ['foodSet'][j]['relatedProductionPlace'],
                description: result.data['allFoodCategory'][i]['foodSet'][j]
                    ['description'],
                category: result.data['allFoodCategory'][i]['id'],
                discount: result.data['allFoodCategory'][i]['foodSet'][j]
                    ['discount'],
                discountPercent: result.data['allFoodCategory'][i]['foodSet'][j]
                    ['discountPercent'],
                status: result.data['allFoodCategory'][i]['foodSet'][j]
                    ['status']);
            List<FoodIngredients> _foodIngredientList = [];
            for (var l = 0; l < result.data['allFoodCategory'][i]['foodSet'][j]['foodingredientsSet'].length; l++) {
              FoodIngredients foodIngredient = FoodIngredients(
                foodId: result.data['allFoodCategory'][i]['foodSet'][j]['id'],
                id: result
                    .data['allFoodCategory'][i]['foodSet'][j]['foodingredientsSet'][l]['id'],
                amount: result
                    .data['allFoodCategory'][i]['foodSet'][j]['foodingredientsSet'][l]['amount'],
                ingredientId: result
                    .data['allFoodCategory'][i]['foodSet'][j]['foodingredientsSet'][l]['ingredient']['id'],
              );
              _foodIngredientList.add(foodIngredient);
            }
            _foodsList.add({'food':food, 'ingredients':_foodIngredientList});
          }

          _loadedFoodCategory.add({'category': _category, 'foods': _foodsList});
        }
        notifyListeners();
      } else {
        throw HttpException(result.exception.toString());
      }
      QueryResult resultUndefined = await _client.query(
        QueryOptions(documentNode: gql(QueryAll().getFoodWithoutCategory())),
      );
      if (!resultUndefined.hasException) {
        FoodCategory _categoryUndefined = FoodCategory('-1', 'Undefined');
        final List<Map<String, Object>> _foodsList = [];
        for (var k = 0;
            k < resultUndefined.data['allFoodWithoutCategory'].length;
            k++) {
          Foods food = Foods(
              id: resultUndefined.data['allFoodWithoutCategory'][k]['id'],
              name: resultUndefined.data['allFoodWithoutCategory'][k]['name'],
              price: resultUndefined.data['allFoodWithoutCategory'][k]['price'],
              relatedProductionPlace: resultUndefined
                  .data['allFoodWithoutCategory'][k]['relatedProductionPlace'],
              description: resultUndefined.data['allFoodWithoutCategory'][k]
                  ['description'],
              category: '-1',
              discount: resultUndefined.data['allFoodWithoutCategory'][k]
                  ['discount'],
              discountPercent: resultUndefined.data['allFoodWithoutCategory'][k]
                  ['discountPercent'],
              status: resultUndefined.data['allFoodWithoutCategory'][k]
                  ['status']);
          List<FoodIngredients> _foodIngredientList = [];
          for (var l = 0; l < resultUndefined.data['allFoodWithoutCategory'][k]['foodingredientsSet'].length; l++) {
            FoodIngredients foodIngredient = FoodIngredients(
              id: resultUndefined
                  .data['allFoodWithoutCategory'][k]['foodingredientsSet'][l]['id'],
              foodId: resultUndefined
                  .data['allFoodWithoutCategory'][k]['id'],
              amount: resultUndefined
                  .data['allFoodWithoutCategory'][k]['foodingredientsSet'][l]['amount'],
              ingredientId: resultUndefined
                  .data['allFoodWithoutCategory'][k]['foodingredientsSet'][l]['ingredient']['id'],
            );
            _foodIngredientList.add(foodIngredient);
          }
          _foodsList.add({'food':food, 'ingredients':_foodIngredientList});
        }
        if(resultUndefined.data['allFoodWithoutCategory'].length > 0) {
          _loadedFoodCategory
              .add({'category': _categoryUndefined, 'foods': _foodsList});
        }
      }
      _categoryList = _loadedFoodCategory;
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  Map<String, Object> getCategoryById(String id){
    Map<String, Object> _categoryIdList;
    _categoryList.forEach((val){
      FoodCategory _cat = val['category'];
      if(_cat.id == id){
        _categoryIdList = val;
      }
    });
    return _categoryIdList;
  }

  Future<void> addFoodCategory(String name) async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult result = await _client.mutate(
        MutationOptions(
            documentNode: gql(MutationsAll().addFoodCategory(name))),
      );
      if (!result.hasException) {
        await fetchAndSetFoodCategory();
      } else {
        throw HttpException(result.exception.toString());
      }
    } catch (error) {
      throw error;
    }
  }

  Future<void> editFoodCategory(String id, String name) async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult result = await _client.mutate(
        MutationOptions(
            documentNode: gql(MutationsAll().editFoodCategory(id, name))),
      );
      if (!result.hasException) {
        await fetchAndSetFoodCategory();
      } else {
        throw HttpException(result.exception.toString());
      }
    } catch (error) {
      throw error;
    }
  }

  Future<void> deleteFoodCategory(String id) async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult result = await _client.mutate(
        MutationOptions(
            documentNode: gql(MutationsAll().deleteFoodCategory(id))),
      );
      if (!result.hasException) {
        await fetchAndSetFoodCategory();
      } else {
        throw HttpException(result.exception.toString());
      }
    } catch (error) {
      throw error;
    }
  }
  Future<void> addFoodIngredient(String foodId, String ingredientId, String amount) async{
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult result = await _client.mutate(
        MutationOptions(documentNode: gql(MutationsAll().addFoodIngredient(foodId, ingredientId, amount))),
      );
      if (!result.hasException) {
        if(result.data['createFoodIngredient']['status']) {
          await fetchAndSetFoodCategory();
        }
        else{
          throw HttpException(result.data['createFoodIngredient']['message']);
        }
      } else {
        throw HttpException(result.exception.toString());
      }
    } catch (error) {
      throw error;
    }
  }

  Future<void> removeFoodIngredient(String id) async{
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult result = await _client.mutate(
        MutationOptions(documentNode: gql(MutationsAll().deleteFoodIngredient(id))),
      );
      if (!result.hasException) {
        await fetchAndSetFoodCategory();
      } else {
        throw HttpException(result.exception.toString());
      }
    } catch (error) {
      throw error;
    }
  }

  Future<void> editFood(String foodId, String name, String price, String categoryId, String discount, String discountPercent, String description, String status, String relatedProductionPlace) async{
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult result = await _client.mutate(
        MutationOptions(documentNode: gql(MutationsAll().editFood(foodId, name, price, categoryId, discount, discountPercent, description, status, relatedProductionPlace))),
      );
      if (!result.hasException) {
        await fetchAndSetFoodCategory();
      } else {
        throw HttpException(result.exception.toString());
      }
    } catch (error) {
      throw error;
    }
  }

  Future<void> addFood(Map<String, Object> _addMap) async{
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult result = await _client.mutate(
        MutationOptions(documentNode: gql(MutationsAll().addFood(_addMap)),
      ));

      if (!result.hasException) {
        List<dynamic> _ingredients = _addMap['ingredientList'];
        for(var i=0; i<_ingredients.length;i++){
          try {
            QueryResult resultIngredients = await _client.mutate(
              MutationOptions(documentNode: gql(MutationsAll().addFoodIngredient(result.data['createFood']['food']['id'], _ingredients[i]['ingredientId'], _ingredients[i]['amount']))),
            );
            if (!resultIngredients.hasException) {
              if(result.data['createFoodIngredient']['status']) {
              }
              else{
                throw HttpException(result.data['createFoodIngredient']['message']);
              }
            } else {
              throw HttpException(result.exception.toString());
            }
          } catch (error) {
            throw error;
          }
          await fetchAndSetFoodCategory();
        }
        await fetchAndSetFoodCategory();
      } else {
        throw HttpException(result.exception.toString());
      }
    } catch (error) {
      throw error;
    }
  }
  Future<void> deleteFood(String id) async{
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(authToken);
    try {
      QueryResult result = await _client.mutate(
        MutationOptions(documentNode: gql(MutationsAll().deleteFood(id))),
      );
      if (!result.hasException) {
        await fetchAndSetFoodCategory();
      } else {
        throw HttpException(result.exception.toString());
      }
    } catch (error) {
      throw error;
    }
  }
}
