import 'package:flutter/foundation.dart';

class Personnel {
  String id;
  String phone;
  String gender;
  String userName;
  String firstName;
  String lastName;
  String email;
  List<Roles> rolesList;

  Personnel(
      {@required this.id,
      @required this.userName,
      @required this.email,
      @required this.phone,
      @required this.rolesList,
      this.firstName,
      this.lastName,
      this.gender});
}

class Permission {
  String id;
  String permission;

  Permission(this.id, this.permission);
}

class Roles {
  String id;
  String name;
  List<Permission> permissionList;
  String roleRelated;

  Roles({@required this.id, @required this.name, @required this.permissionList, this.roleRelated});
}