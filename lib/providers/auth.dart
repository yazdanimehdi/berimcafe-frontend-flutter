import 'dart:async';
import 'dart:convert';
import '../graphql_operation/queries/queries.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../services/graphQLConf.dart';
import 'package:flutter/widgets.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../models/http_exceptions.dart';
import '../graphql_operation/mutations/mutations.dart';

class Auth with ChangeNotifier {
  String _token;
  String _userId;

  bool get isAuth {
    return token != null;
  }

  String get token {
    return _token;
  }

  void _authenticate(String username, String password) async {
    GraphQLClient _client = GraphQLConfiguration().clientToQuery(null);
    QueryResult result = await _client.mutate(MutationOptions(
        documentNode: gql(MutationsAll().getAuthToken(username, password))));
    if (result.hasException) {
      throw HttpException(result.exception.toString());
    } else {
      _token = result.data['tokenAuth']['token'];
      _client = GraphQLConfiguration().clientToQuery(_token);
      QueryResult resultMe = await _client
          .query(QueryOptions(documentNode: gql(QueryAll().getMe())));
      _userId = resultMe.data['me']['id'];
      notifyListeners();
      final prefs = await SharedPreferences.getInstance();
      final userData = json.encode(
        {
          'token': _token,
          'userId': _userId,
        },
      );
      prefs.setString('userData', userData);
    }
  }

  Future<void> login(String username, String password) async {
    return _authenticate(username, password);
  }

//
  Future<void> logout() async {
    _token = null;
    _userId = null;
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('userData');
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
        json.decode(prefs.getString('userData')) as Map<String, Object>;

    _token = extractedUserData['token'];
    _userId = extractedUserData['userId'];
    notifyListeners();
    return true;
  }
}
