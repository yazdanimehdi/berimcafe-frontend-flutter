import 'package:cafe_management_app/screens/tab_view_screen.dart';

import '../../providers/ingredients.dart';
import '../../providers/locale.dart';
import 'package:flutter/services.dart';

import '../../locale/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/foods.dart';
import '../../providers/foods_all.dart';
import '../../models/http_exceptions.dart';

class FoodViewEditScreen extends StatefulWidget {
  static const routeName = '/food-edit';

  @override
  _FoodViewEditScreenState createState() => _FoodViewEditScreenState();
}

class _FoodViewEditScreenState extends State<FoodViewEditScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  final GlobalKey<FormState> _form2Key = GlobalKey();
  var _isEditing = false;
  var _isInit = true;
  var _isLoading = false;
  String dropdownValue;
  String amount;
  String dropdownValueProductionPlace;
  String dropdownValueStatus;
  String productionPlaceValue;
  String statusValue;
  String categoryId;
  String name;
  String price;
  String discount;
  String discountPercent;
  String description;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<IngredientsProvider>(context).getAllIngredients();
      Provider.of<FoodsAll>(context).fetchAndSetFoodCategory().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context).settings.arguments as Map;
    final List<String> _categoryList =
        Provider.of<FoodsAll>(context).getCategoriesList['names'];
    final List<String> _categoryListId =
        Provider.of<FoodsAll>(context).getCategoriesList['ids'];
    final List<Map<String, Object>> _foods = Provider.of<FoodsAll>(context)
        .getCategories[arguments['category']]['foods'];
    final Foods _food = _foods[arguments['index']]['food'];
    final List<FoodIngredients> _ingredients =
        _foods[arguments['index']]['ingredients'];
    final FoodCategory _category = Provider.of<FoodsAll>(context)
        .getCategories[arguments['category']]['category'];
    final MaterialColor _color = arguments['color'];
    final List<String> _productionPlace = [
      AppLocalizations.of(context).translate('kitchen'),
      AppLocalizations.of(context).translate('bar')
    ];
    final List<String> _status = [
      AppLocalizations.of(context).translate('enable'),
      AppLocalizations.of(context).translate("disable")
    ];
    if (_isInit) {
      dropdownValue = _category.name;
      if (_food.relatedProductionPlace == 'KI') {
        dropdownValueProductionPlace = _productionPlace[0];
        productionPlaceValue = 'KI';
      } else {
        dropdownValueProductionPlace = _productionPlace[1];
        productionPlaceValue = 'BA';
      }
      if (_food.status) {
        dropdownValueStatus = _status[0];
        statusValue = true.toString();
        print(statusValue);
      } else {
        dropdownValueStatus = _status[1];
        statusValue = false.toString();
      }
      price = _food.price.toString();


      _isInit = false;
    }
    void _showErrorDialog(String message) {
      showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title:
              Text(AppLocalizations.of(context).translate('login_error_title')),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(ctx).pop();
              },
            )
          ],
        ),
      );
    }

    Future<void> _submit(String foodId, String ingredientId) async {
      if (!_form2Key.currentState.validate()) {
        // Invalid!
        return;
      }
      _form2Key.currentState.save();
      setState(() {
        _isLoading = true;
      });
      try {
        await Provider.of<FoodsAll>(context, listen: false)
            .addFoodIngredient(foodId, ingredientId, amount);
      } on HttpException catch (error) {
        print(error.toString());
        var errorMessage = 'Editing failed';
        if (error.toString().contains('Please, enter valid credentials')) {
          errorMessage =
              AppLocalizations.of(context).translate('login_error_message');
        }
        _showErrorDialog(errorMessage);
      } catch (error) {
        print(error);
        var errorMessage =
            AppLocalizations.of(context).translate('connection_error');
        _showErrorDialog(errorMessage);
      }

      setState(() {
        _isLoading = false;
      });
      Navigator.of(context, rootNavigator: true)..pop();
    }

    Future<void> _submitEditFood(String foodId) async {
      if (!_formKey.currentState.validate()) {
        // Invalid!
        return;
      }
      _formKey.currentState.save();
      setState(() {
        _isLoading = true;
      });
      try {
        await Provider.of<FoodsAll>(context, listen: false).editFood(
            foodId,
            name,
            price,
            categoryId,
            discount,
            discountPercent,
            description,
            statusValue,
            productionPlaceValue);
      } on HttpException catch (error) {
        print(error.toString());
        var errorMessage = 'Editing failed';
        if (error.toString().contains('Please, enter valid credentials')) {
          errorMessage =
              AppLocalizations.of(context).translate('login_error_message');
        }
        _showErrorDialog(errorMessage);
      } catch (error) {
        print(error);
        var errorMessage =
            AppLocalizations.of(context).translate('connection_error');
        _showErrorDialog(errorMessage);
      }

      setState(() {
        _isLoading = false;
      });
      Navigator.of(context).pushReplacementNamed(TabViewScreen.routeName);
    }

    void _addIngredient(String ingredientCat, String ingredient) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            String dropdownValueCategory = ingredientCat;
            String dropdownValueIngredient = ingredient;
            Map<String, List<Object>> ingredientName;
            List<String> ingredients;
            String ingredientId;
            final List<String> _ingredientCat =
                Provider.of<IngredientsProvider>(context, listen: false)
                    .getCategories['names'];
            return AlertDialog(
              content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) =>
                    SingleChildScrollView(
                  child: Form(
                    key: _form2Key,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(AppLocalizations.of(context)
                            .translate('add_ingredient')),
                        Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Column(
                              children: <Widget>[
                                Text(AppLocalizations.of(context)
                                    .translate('ingredientsـcategory')),
                                DropdownButton(
                                  value: dropdownValueCategory,
                                  icon: Icon(Icons.arrow_downward),
                                  iconSize: 24,
                                  elevation: 16,
                                  onChanged: (String newValue) {
                                    int _index =
                                        _ingredientCat.indexOf(newValue);
                                    setState(() {
                                      ingredientName = Provider.of<
                                                  IngredientsProvider>(context,
                                              listen: false)
                                          .getIngredientsByCategory(
                                              Provider.of<IngredientsProvider>(
                                                          context,
                                                          listen: false)
                                                      .getCategories['ids']
                                                  [_index]);
                                      ingredients = ingredientName['names'];
                                      dropdownValueCategory = newValue;
                                    });
                                  },
                                  items: _ingredientCat
                                      .map<DropdownMenuItem<String>>(
                                          (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                                Text(AppLocalizations.of(context)
                                    .translate('ingredient')),
                                DropdownButton(
                                  value: dropdownValueIngredient,
                                  icon: Icon(Icons.arrow_downward),
                                  iconSize: 24,
                                  elevation: 16,
                                  onChanged: ingredientName == null
                                      ? (_) {}
                                      : (String newValue) {
                                          int _index = ingredientName['names']
                                              .indexOf(newValue);

                                          setState(() {
                                            dropdownValueIngredient = newValue;
                                            ingredientId =
                                                ingredientName['ids'][_index];
                                          });
                                        },
                                  items: ingredients == null
                                      ? null
                                      : ingredients
                                          .map<DropdownMenuItem<String>>(
                                              (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(value),
                                          );
                                        }).toList(),
                                ),
                                TextFormField(
                                  validator: (value){
                                    int _intValue;
                                    try{
                                      _intValue = int.parse(value);
                                      return null;
                                    }
                                    catch(error){
                                      return AppLocalizations.of(context)
                                          .translate('input_english');
                                    }
                                  },
                                  keyboardType: TextInputType.number,
                                  onSaved: (value) {
                                    amount = value;
                                  },
                                  decoration: InputDecoration(
                                    labelText: AppLocalizations.of(context)
                                        .translate('amount'),
                                  ),
                                )
                              ],
                            )),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: RaisedButton(
                              child: Text(AppLocalizations.of(context)
                                  .translate('submit')),
                              onPressed: () {
                                return _submit(_food.id, ingredientId);
                              }),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            );
          });
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(_food.name),
        backgroundColor: _color,
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              setState(() {
                _isEditing = !_isEditing;
              });
            },
            child: Text(
              AppLocalizations.of(context).translate('edit'),
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      ),
      body: _isLoading
          ? Center(child: CircularProgressIndicator())
          : _isEditing
              ? Form(
                  key: _formKey,
                  child: Padding(
                    padding: EdgeInsets.all(20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            CircleAvatar(
                              child: Text(
                                _food.name[0],
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 30),
                              ),
                              backgroundColor: _color,
                              maxRadius: 50,
                              minRadius: 20,
                            ),
                            Container(
                              width: 200,
                              child: TextFormField(
                                initialValue: _food.name,
                                decoration: InputDecoration(
                                    labelText: AppLocalizations.of(context)
                                        .translate('name')),
                                onSaved: (value) {
                                  name = value;
                                },
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Expanded(
                          child: Container(
                              child: ListView(
                            children: <Widget>[
                              TextFormField(
                                validator: (value){
                                  int _intValue;
                                  try{
                                    _intValue = int.parse(value);
                                    return null;
                                  }
                                  catch(error){
                                    return AppLocalizations.of(context)
                                        .translate('input_english');
                                  }
                                },
                                decoration: InputDecoration(
                                  labelText: AppLocalizations.of(context)
                                      .translate('price'),
                                ),
                                onSaved: (value) {
                                  price = value;
                                },
                                initialValue: _food.price.toString(),
                                keyboardType: TextInputType.number,
                                textInputAction: TextInputAction.done,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(AppLocalizations.of(context)
                                      .translate('category')),
                                  DropdownButton(
                                    value: dropdownValue,
                                    icon: Icon(Icons.arrow_downward),
                                    iconSize: 24,
                                    elevation: 16,
                                    onChanged: (String newValue) {
                                      setState(() {
                                        dropdownValue = newValue;
                                        categoryId = _categoryListId[
                                            _categoryList.indexOf(newValue)];
                                      });
                                    },
                                    items: _categoryList
                                        .map<DropdownMenuItem<String>>(
                                            (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                  ),
                                ],
                              ),
                              TextFormField(
                                
                                decoration: InputDecoration(
                                  labelText: AppLocalizations.of(context)
                                      .translate('discount'),
                                ),
                                initialValue: _food.discount == null
                                    ? '0'
                                    : _food.discount.toString(),
                                keyboardType: TextInputType.number,
                                onSaved: (value) {
                                  discount = value;
                                },
                                validator: (value) {
                                    int _intValue;
                                    try{
                                      _intValue = int.parse(value);
                                    }
                                    catch(error){
                                      return AppLocalizations.of(context)
                                          .translate('input_english');
                                    }
                                  if (int.parse(value) > _food.price) {
                                    return '';
                                  } else {
                                    return null;
                                  }
                                },
                              ),
                              TextFormField(
                                validator: (value){
                                  int _intValue;
                                  try{
                                    _intValue = int.parse(value);
                                    if (_intValue < 100) {
                                      return null;
                                    }
                                    else{
                                      return AppLocalizations.of(context).translate('input_field_valid');
                                    }
                                  }
                                  catch(error){
                                    return AppLocalizations.of(context)
                                        .translate('input_english');
                                  }
                                },
                                decoration: InputDecoration(
                                  labelText: AppLocalizations.of(context)
                                      .translate('discount_percent'),
                                ),
                                keyboardType: TextInputType.number,
                                onSaved: (value) {
                                  discountPercent = value;
                                },
                                initialValue: _food.discountPercent == null
                                    ? '0'
                                    : _food.discountPercent.toString(),
                              ),
                              Text(AppLocalizations.of(context)
                                  .translate('status')),
                              DropdownButton(
                                value: dropdownValueStatus,
                                icon: Icon(Icons.arrow_downward),
                                iconSize: 24,
                                elevation: 16,
                                onChanged: (String newValue) {
                                  setState(() {
                                    dropdownValueStatus = newValue;
                                    if (_status.indexOf(newValue) == 0) {
                                      statusValue = true.toString();
                                    } else {
                                      statusValue = false.toString();
                                    }
                                  });
                                },
                                items: _status.map<DropdownMenuItem<String>>(
                                    (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                              ),
                              Text(AppLocalizations.of(context)
                                  .translate('related_production_place')),
                              DropdownButton(
                                value: dropdownValueProductionPlace,
                                icon: Icon(Icons.arrow_downward),
                                iconSize: 24,
                                elevation: 16,
                                onChanged: (String newValue) {
                                  setState(() {
                                    dropdownValueProductionPlace = newValue;
                                    if (_productionPlace.indexOf(newValue) ==
                                        0) {
                                      productionPlaceValue = 'KI';
                                    } else {
                                      productionPlaceValue = "BA";
                                    }
                                  });
                                },
                                items: _productionPlace
                                    .map<DropdownMenuItem<String>>(
                                        (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                              ),
                              TextFormField(
                                decoration: InputDecoration(
                                  labelText: AppLocalizations.of(context)
                                      .translate('description'),
                                ),
                                onSaved: (value) {
                                  description = value;
                                },
                                keyboardType: TextInputType.multiline,
                                initialValue: _food.description,
                                maxLines: 4,
                              ),
                              RaisedButton(
                                child: Text(AppLocalizations.of(context)
                                    .translate('submit')),
                                onPressed: () => _submitEditFood(_food.id),
                              ),
                              Divider(),
                              Container(
                                height: 200,
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(AppLocalizations.of(context)
                                            .translate('ingredient')),
                                        IconButton(
                                          icon: Icon(
                                            Icons.add,
                                            color: Colors.green,
                                          ),
                                          onPressed: () =>
                                              _addIngredient(null, null),
                                        )
                                      ],
                                    ),
                                    Expanded(
                                      child: Consumer<IngredientsProvider>(
                                        builder: (ctx, ingredient, _) =>
                                            ListView.separated(
                                                itemBuilder: (BuildContext ctx,
                                                    int index) {
                                                  Ingredients _ingredientInst =
                                                      ingredient
                                                          .getIngredientsById(
                                                              _ingredients[
                                                                      index]
                                                                  .ingredientId);
                                                  return Dismissible(
                                                    direction:
                                                        Provider.of<LocaleProvider>(
                                                                        context)
                                                                    .locale ==
                                                                Locale(
                                                                    'en', 'US')
                                                            ? DismissDirection
                                                                .endToStart
                                                            : DismissDirection
                                                                .startToEnd,
                                                    key: ValueKey(
                                                        _ingredients[index].id),
                                                    onDismissed: (direction) {
                                                      Provider.of<FoodsAll>(
                                                              context,
                                                              listen: false)
                                                          .removeFoodIngredient(
                                                              _ingredients[
                                                                      index]
                                                                  .id);
                                                      _ingredients
                                                          .removeAt(index);
                                                    },
                                                    background: Container(
                                                      height: 100,
                                                      color: Theme.of(context)
                                                          .errorColor,
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(top: 10),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: <Widget>[
                                                            Icon(
                                                              Icons.delete,
                                                              color:
                                                                  Colors.white,
                                                              size: 22,
                                                            ),
                                                            SizedBox(
                                                              height: 2,
                                                            ),
                                                            Text(
                                                              AppLocalizations.of(
                                                                      context)
                                                                  .translate(
                                                                      'delete'),
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      alignment:
                                                          Alignment.centerRight,
                                                      padding: EdgeInsets.only(
                                                          right: 20),
                                                    ),
                                                    child: ListTile(
                                                      title: Text(
                                                          _ingredientInst.name),
                                                      trailing: Column(
                                                        children: <Widget>[
                                                          Text(_ingredients[
                                                                  index]
                                                              .amount
                                                              .toString()),
                                                          Text(_ingredientInst
                                                              .scale)
                                                        ],
                                                      ),
                                                    ),
                                                  );
                                                },
                                                separatorBuilder:
                                                    (context, index) => Divider(
                                                          height: 20,
                                                          color: Colors.black26,
                                                        ),
                                                itemCount: _ingredients.length),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          )),
                        ),
                      ],
                    ),
                  ))
              : Padding(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          CircleAvatar(
                            child: Text(
                              _food.name[0],
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 30),
                            ),
                            backgroundColor: _color,
                            maxRadius: 50,
                            minRadius: 20,
                          ),
                          Container(
                            width: 200,
                            child: Text(
                              _food.name,
                              style: TextStyle(
                                  fontSize: 28, fontWeight: FontWeight.bold),
                              textAlign: TextAlign.start,
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Expanded(
                        child: Container(
                            child: ListView(
                          children: <Widget>[
                            ListTile(
                              title: Text(AppLocalizations.of(context)
                                  .translate('price')),
                              trailing: Text(_food.price.toString()),
                            ),
                            ListTile(
                              title: Text(AppLocalizations.of(context)
                                  .translate(('category'))),
                              trailing: Text(_category.name),
                            ),
                            ListTile(
                              title: Text(AppLocalizations.of(context)
                                  .translate(('discount'))),
                              trailing: Text(_food.discount.toString()),
                            ),
                            ListTile(
                              title: Text(AppLocalizations.of(context)
                                  .translate(('discount_percent'))),
                              trailing: Text(_food.discountPercent.toString()),
                            ),
                            ListTile(
                              title: Text(AppLocalizations.of(context)
                                  .translate(('status'))),
                              trailing: _food.status == true
                                  ? Text(
                                      AppLocalizations.of(context)
                                          .translate('enable'),
                                      style: TextStyle(color: Colors.green),
                                    )
                                  : Text(
                                      AppLocalizations.of(context)
                                          .translate('disable'),
                                      style: TextStyle(color: Colors.red),
                                    ),
                            ),
                            ListTile(
                              title: Text(AppLocalizations.of(context)
                                  .translate(('related_production_place'))),
                              trailing: Text(_food.relatedProductionPlace == 'KI'? AppLocalizations.of(context).translate('kitchen'): AppLocalizations.of(context).translate('bar')),
                            ),
                            ListTile(
                              title: Text(AppLocalizations.of(context)
                                  .translate(('description'))),
                              trailing: Text(_food.description),
                            ),
                            Divider(),
                            Container(
                              height: 200,
                              child: Column(
                                children: <Widget>[
                                  Text(AppLocalizations.of(context)
                                      .translate('ingredient')),
                                  Expanded(
                                    child: Consumer<IngredientsProvider>(
                                      builder: (ctx, ingredient, _) =>
                                          ListView.separated(
                                              itemBuilder: (BuildContext ctx,
                                                  int index) {
                                                Ingredients _ingredientInst =
                                                    ingredient
                                                        .getIngredientsById(
                                                            _ingredients[index]
                                                                .ingredientId);
                                                return ListTile(
                                                  title: Text(
                                                      _ingredientInst.name),
                                                  trailing: Column(
                                                    children: <Widget>[
                                                      Text(_ingredients[index]
                                                          .amount
                                                          .toString()),
                                                      Text(
                                                          _ingredientInst.scale)
                                                    ],
                                                  ),
                                                );
                                              },
                                              separatorBuilder:
                                                  (context, index) => Divider(
                                                        height: 20,
                                                        color: Colors.black26,
                                                      ),
                                              itemCount: _ingredients.length),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        )),
                      ),
                    ],
                  ),
                ),
    );
  }
}
